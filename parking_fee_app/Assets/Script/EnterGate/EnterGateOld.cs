﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using UnityEngine.UI;

public class EnterGateOld : MonoBehaviour {

    public Text enterDate;
    public Text enterTime;
    public Text stampDate;
    public Text stampTime;
    public Text stampCode;
    public Text stampSector;
    public Text stationDate;
    public Text stationTime;
    public Text stationValue;
    public Text stationSector;

    private byte[] passINNET = new byte[] { 0x49, 0x4E, 0x4E, 0x45, 0x54, 0x39 };
    private byte[] passDefault = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void sendMsg(string msg)
    {
        Debug.Log("send from java " + msg);
    }

    public void JavaSendMessage(string result)
    {
        Debug.Log("unity send msg :" + result);
        if (result.Contains("TAG_DISCOVERED"))
        {
            // tag present 
            //ReadTagProcess();
            //Debug.Log("tag present " + result);
        }
    }

    public void WriteData(int block, int sector, byte[] data)
    {
        block = 2;
        sector = 8;
        byte[] dataReadArr = ReadTagBySector(sector);
        string writeData = ByteArrToString(dataReadArr, block);
        Debug.Log("write data : " + writeData);
        WriteTagProcess(block, sector, writeData);
    }

    public byte[] ReadTagBySector(int sector)
    {
        byte[] dataReadArr = new byte[0];
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            string[] dataReadBuffer = ajc.CallStatic<string[]>("ReadTagReturnStringArray", sector, passINNET);
            dataReadArr = ToByteArray(dataReadBuffer[0] + dataReadBuffer[1] + dataReadBuffer[2] + dataReadBuffer[3]);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
        }

        return dataReadArr;
    }

    public void WriteTagProcess(int block, int sector, string data)
    {
        //CommonFn.callLoading(true);
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            //public static int writeTagClick(int selBlock,int selSector,String data,byte[] pass)
            //string dummyData = "F1E2D3C4B5A60708090A0B0C0D0E0F33";
            /*string dummyData = "11E2D3C4B5A60708090A0B0C0D0E0F55"*/
            ;
            int vals = ajc.CallStatic<int>("writeTagClick", block, sector, data, passINNET);
            Debug.Log("WriteTagProcess = " + vals);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            //CommonFn.callLoading(false);
            
        }
        //CommonFn.callLoading(false);
        
    }

    private byte[] ToByteArray(string HexString)
    {
        int NumberChars = HexString.Length;
        byte[] bytes = new byte[NumberChars / 2];
        for (int i = 0; i < NumberChars; i += 2)
        {
            //bytes[i / 2] = Convert.ToByte(HexString.Substring(i, 2), 16);
            bytes[i / 2] = (byte)Convert.ToInt32(HexString.Substring(i, 2), 16);
            //int intBuf = Convert.ToInt32(HexString.Substring(i, 2), 16);
        }
        return bytes;
    }

    private string ByteArrToString(byte[] data, int block)
    {
        StringBuilder sb = new StringBuilder(data.Length * 2);
        foreach (byte b in data)
        {
            sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
        }
        return (sb.ToString().ToUpper()).Substring(block * 32, 32);
    }

    public void EnterGate ()
    {
        try
        {
            Debug.Log("set time enter");
            int sector = 8;
            int block = 2;

            //read data from card
            byte[] dataReadArr = ReadTagBySector(sector);
            if(dataReadArr.Length <= 0)
            {
                Debug.Log("read sector 8 block 2 fail");
                return;
            }

            Debug.Log("dataReadArr lenght = " + dataReadArr.Length);

            Debug.Log("date = " + enterDate.text.Substring(0, 2));
            Debug.Log("month = " + enterDate.text.Substring(2, 2));
            Debug.Log("year = " + enterDate.text.Substring(4, 2));
            Debug.Log("hour = " + enterDate.text.Substring(0, 2));
            Debug.Log("min = " + enterDate.text.Substring(2, 2));
            Debug.Log("sec = " + enterDate.text.Substring(4, 2));
            //change data
            dataReadArr[36] = Convert.ToByte(enterDate.text.Substring(0, 2));
            dataReadArr[37] = Convert.ToByte(enterDate.text.Substring(2, 2));
            dataReadArr[38] = Convert.ToByte(enterDate.text.Substring(4, 2));
            dataReadArr[39] = Convert.ToByte(enterTime.text.Substring(0, 2));
            dataReadArr[40] = Convert.ToByte(enterTime.text.Substring(2, 2));
            dataReadArr[41] = Convert.ToByte(enterTime.text.Substring(4, 2));

            //write data to card
            string writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, sector, writeData);

            //write card type
            dataReadArr[4] = 0x01;
            string writeDataFirstBlock = ByteArrToString(dataReadArr, 0);
            Debug.Log("write data first sector: " + writeDataFirstBlock);
            WriteTagProcess(0, sector, writeDataFirstBlock);

            ToastrControl.toastrBackground = ToastrControl.toastrSuccess;
            ToastrControl.toastrMessage = "Write Date Time Success";
            ToastrControl.toastrShow = true;
        }
        catch(Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    public void WriteStamp ()
    {
        int block = 0;
        bool isSectorOk = StampSectorLimit();
        if (isSectorOk)
        {
            int sector = Convert.ToInt32(stampSector.text);
            byte[] dataReadArr = ReadTagBySector(sector);

            //change data
            dataReadArr[0] = Convert.ToByte(stampDate.text.Substring(0, 2));
            dataReadArr[1] = Convert.ToByte(stampDate.text.Substring(2, 2));
            dataReadArr[2] = Convert.ToByte(stampDate.text.Substring(4, 2));
            dataReadArr[3] = Convert.ToByte(stampTime.text.Substring(0, 2));
            dataReadArr[4] = Convert.ToByte(stampTime.text.Substring(2, 2));
            dataReadArr[5] = Convert.ToByte(stampTime.text.Substring(4, 2));

            dataReadArr[10] = Convert.ToByte((Convert.ToInt16(stampCode.text) & 0xff00) >> 8);
            dataReadArr[11] = Convert.ToByte(Convert.ToInt16(stampCode.text)  & 0x00ff);

            //write data to card
            string writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, sector, writeData);

            CheckAllStamp();

            ToastrControl.toastrBackground = ToastrControl.toastrSuccess;
            ToastrControl.toastrMessage = "Write Stamp Success";
            ToastrControl.toastrShow = true;
        }
    }

    public void ClearStampSector()
    {
        int block = 0;
        bool isSectorOk = StampSectorLimit();
        if (isSectorOk)
        {
            int sector = Convert.ToInt32(stampSector.text);
            byte[] dataReadArr = ReadTagBySector(sector);

            //change data
            dataReadArr[0] = Convert.ToByte(0);
            dataReadArr[1] = Convert.ToByte(0);
            dataReadArr[2] = Convert.ToByte(0);
            dataReadArr[3] = Convert.ToByte(0);
            dataReadArr[4] = Convert.ToByte(0);
            dataReadArr[5] = Convert.ToByte(0);

            dataReadArr[10] = Convert.ToByte(0);
            dataReadArr[11] = Convert.ToByte(0);

            //write data to card
            string writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, sector, writeData);

            CheckAllStamp();

            ToastrControl.toastrBackground = ToastrControl.toastrSuccess;
            ToastrControl.toastrMessage = "Clear Stamp Success";
            ToastrControl.toastrShow = true;
        }
    }

    public void WriteStationPaid ()
    {
        int block = 0;
        bool isSectorOk = StationSectorLimit();
        if (isSectorOk)
        {
            int sector = Convert.ToInt32(stationSector.text);
            byte[] dataReadArr = ReadTagBySector(sector);

            //change data
            dataReadArr[2] = Convert.ToByte(stationDate.text.Substring(0, 2));
            dataReadArr[3] = Convert.ToByte(stationDate.text.Substring(2, 2));
            dataReadArr[4] = Convert.ToByte(stationDate.text.Substring(4, 2));
            dataReadArr[5] = Convert.ToByte(stationTime.text.Substring(0, 2));
            dataReadArr[6] = Convert.ToByte(stationTime.text.Substring(2, 2));
            dataReadArr[7] = Convert.ToByte(stationTime.text.Substring(4, 2));

            dataReadArr[8] =  Convert.ToByte((Convert.ToInt16(stationValue.text) & 0xff000000) >> 32);
            dataReadArr[9] =  Convert.ToByte((Convert.ToInt16(stationValue.text) & 0x00ff0000) >> 16);
            dataReadArr[10] = Convert.ToByte((Convert.ToInt16(stationValue.text) & 0x0000ff00) >> 8);
            dataReadArr[11] = Convert.ToByte(Convert.ToInt16(stationValue.text)  & 0x000000ff);

            //write data to card
            string writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, sector, writeData);

            CheckAllPaid();

            ToastrControl.toastrBackground = ToastrControl.toastrSuccess;
            ToastrControl.toastrMessage = "Write Station Success";
            ToastrControl.toastrShow = true;
        }
    }

    public void ClearStationPaidSector ()
    {
        int block = 0;
        bool isSectorOk = StationSectorLimit();
        if (isSectorOk)
        {
            int sector = Convert.ToInt32(stationSector.text);
            byte[] dataReadArr = ReadTagBySector(sector);

            //change data
            dataReadArr[2] = Convert.ToByte(0);
            dataReadArr[3] = Convert.ToByte(0);
            dataReadArr[4] = Convert.ToByte(0);
            dataReadArr[5] = Convert.ToByte(0);
            dataReadArr[6] = Convert.ToByte(0);
            dataReadArr[7] = Convert.ToByte(0);

            dataReadArr[8] = Convert.ToByte(0);
            dataReadArr[9] = Convert.ToByte(0);
            dataReadArr[10] = Convert.ToByte(0);
            dataReadArr[11] = Convert.ToByte(0);

            //write data to card
            string writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, sector, writeData);

            CheckAllPaid();

            ToastrControl.toastrBackground = ToastrControl.toastrSuccess;
            ToastrControl.toastrMessage = "Clear Station Success";
            ToastrControl.toastrShow = true;
        }
    }

    public void ClearAll ()
    {
        ////////////////////////// sector 8 /////////////////////////////
        int sector = 8;
        int block = 1; 

        //read data from card
        byte[] dataReadArr = ReadTagBySector(sector);

        //change data
        dataReadArr[17] = Convert.ToByte(0);
        dataReadArr[18] = Convert.ToByte(0);
        dataReadArr[19] = Convert.ToByte(0);

        //write data to card
        string writeData = ByteArrToString(dataReadArr, block);
        Debug.Log("write data : " + writeData);
        WriteTagProcess(block, sector, writeData);

        block = 2;

        //change data
        dataReadArr[36] = Convert.ToByte(0);
        dataReadArr[37] = Convert.ToByte(0);
        dataReadArr[38] = Convert.ToByte(0);
        dataReadArr[39] = Convert.ToByte(0);
        dataReadArr[40] = Convert.ToByte(0);
        dataReadArr[41] = Convert.ToByte(0);

        dataReadArr[42] = Convert.ToByte(0);

        //write data to card
        writeData = ByteArrToString(dataReadArr, block);
        Debug.Log("write data : " + writeData);
        WriteTagProcess(block, sector, writeData);

        ////////////////////////// sector 3-7 9-15 /////////////////////////////
        block = 0;
        int[] stampSectorClear = new int[] { 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15 };

        for (int i = 0; i < stampSectorClear.Length; i++)
        {
            dataReadArr = ReadTagBySector(stampSectorClear[i]);

            //change data
            dataReadArr[0] = Convert.ToByte(0);
            dataReadArr[1] = Convert.ToByte(0);
            dataReadArr[2] = Convert.ToByte(0);
            dataReadArr[3] = Convert.ToByte(0);
            dataReadArr[4] = Convert.ToByte(0);
            dataReadArr[5] = Convert.ToByte(0);

            dataReadArr[10] = Convert.ToByte(0);
            dataReadArr[11] = Convert.ToByte(0);

            //write data to card
            writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, stampSectorClear[i], writeData);
        }

        ////////////////////////// sector 1-2 /////////////////////////////
        block = 0;
        int[] stationSectorClear = new int[] { 1, 2 };

        for (int i = 0; i < stationSectorClear.Length; i++)
        {
            dataReadArr = ReadTagBySector(stationSectorClear[i]);

            //change data
            dataReadArr[2] = Convert.ToByte(0);
            dataReadArr[3] = Convert.ToByte(0);
            dataReadArr[4] = Convert.ToByte(0);
            dataReadArr[5] = Convert.ToByte(0);
            dataReadArr[6] = Convert.ToByte(0);
            dataReadArr[7] = Convert.ToByte(0);

            dataReadArr[8] = Convert.ToByte(0);
            dataReadArr[9] = Convert.ToByte(0);
            dataReadArr[10] = Convert.ToByte(0);
            dataReadArr[11] = Convert.ToByte(0);

            //write data to card
            writeData = ByteArrToString(dataReadArr, block);
            Debug.Log("write data : " + writeData);
            WriteTagProcess(block, stationSectorClear[i], writeData);
        }

        ToastrControl.toastrBackground = ToastrControl.toastrSuccess;
        ToastrControl.toastrMessage = "Clear All Success";
        ToastrControl.toastrShow = true;
    }

    public bool StampSectorLimit ()
    {
        bool result = false;
        uint stampSectorBuffer = Convert.ToUInt16(stampSector.text);
        if (!((stampSectorBuffer >= 3 && stampSectorBuffer <= 7) || (stampSectorBuffer >= 9 && stampSectorBuffer <= 15)))
        {
            ToastrControl.toastrBackground = ToastrControl.toastrWarning;
            ToastrControl.toastrMessage = "Please input sector 3-7, 9-15 only";
            ToastrControl.toastrShow = true;
        }
        else
        {
            result = true;
        }

        return result;
    }

    public bool StationSectorLimit()
    {
        bool result = false;
        uint stationSectorBuffer = Convert.ToUInt16(stationSector.text);
        if (!((stationSectorBuffer >= 1 && stationSectorBuffer <= 2)))
        {
            ToastrControl.toastrBackground = ToastrControl.toastrWarning;
            ToastrControl.toastrMessage = "Please input sector 1-2 only";
            ToastrControl.toastrShow = true;
        }
        else
        {
            result = true;
        }

        return result;
    }

    public void CheckAllStamp ()
    {
        //check all stamp
        int block = 0;
        int[] stampSectorCheck = new int[] { 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15 };
        byte[] dataReadArr;
        int totalStamp = 0;

        for (int i = 0; i < stampSectorCheck.Length; i++)
        {
            dataReadArr = ReadTagBySector(stampSectorCheck[i]);

            if (dataReadArr[1] != 0) //check time stamp exist
            {
                totalStamp++;
            }
        }

        //read parking info
        block = 1;
        int sector = 8;

        dataReadArr = ReadTagBySector(sector);

        //change total stamp counter
        dataReadArr[17] = Convert.ToByte(totalStamp);

        //write data to card
        string writeData = ByteArrToString(dataReadArr, block);
        Debug.Log("stamp counter total data : " + writeData);
        WriteTagProcess(block, sector, writeData);
    }

    public void CheckAllPaid()
    {
        //check all stamp
        int block = 0;
        int[] stationSectorCheck = new int[] { 1, 2 };
        byte[] dataReadArr;
        int totalStation = 0;

        for (int i = 0; i < stationSectorCheck.Length; i++)
        {
            dataReadArr = ReadTagBySector(stationSectorCheck[i]);

            if(dataReadArr[3] != 0) //check time stamp exist
            {
                totalStation++;
            }
        }

        //read parking info
        block = 2;
        int sector = 8;

        dataReadArr = ReadTagBySector(sector);

        //change total stamp counter
        dataReadArr[42] = Convert.ToByte(totalStation);

        //write data to card
        string writeData = ByteArrToString(dataReadArr, block);
        Debug.Log("stamp counter total data : " + writeData);
        WriteTagProcess(block, sector, writeData);
    }

}
