﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainEventEnterGate : MonoBehaviour {

    public InputField licensePrefixInput;
    public InputField licenseCodeInput;
    public Text UserTx;
    System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

    void Start()
    {
        try
        {
            licensePrefixInput.characterLimit = 3;
            licenseCodeInput.characterLimit = 4;
            Config.LoadXmlToDataTable();
            Debug.Log($"check parking card {Config.dtParkingCard.Rows.Count}");
            stopwatch.Start();
            UserTx.text = "ผู้ใช้งานระบบ: " + ShareInstance.userName;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเริ่มต้นทำงาน");
        }
    }

    void Update()
    {
        if (stopwatch.Elapsed.TotalSeconds >= 5)
        {
            stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            Transaction.ResendFailTransaction();
        }

        if (Application.platform == RuntimePlatform.Android)
        {

            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {

                // Quit the application
                Application.Quit();
            }
        }
    }

    public void BackBtClick()
	{
        //SceneManager.LoadScene("SelectMode", LoadSceneMode.Additive);
        //SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene(Constant.SelectMode);
    }

    #region write card
    //Event when card is read
    public void OnNewIntentEvent(string result)
    {
        try
        {
            Debug.Log($"onNewIntentEvent = {result}");
            if (result == Constant.mifareClassic)
            {
                string uid = CardData.GetUID();
                //var uid = "FA2A9B26";
                Debug.Log($"uid = {uid}");
                if (string.IsNullOrEmpty(uid)) CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่าน UID ของบัตรได้");
                bool IsCardActive = CardData.IsCardActive(uid);
                if (!IsCardActive) CommonFn.ShowToast(ToastrControl.toastrError, "บัตรถูกระงับการใช้งาน");
                string cardType = CardData.GetCardType(uid);
                if (string.IsNullOrEmpty(cardType)) CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่อยู่ในระบบ");
                Debug.Log($"cardType = {cardType}");
                string cardGroup = CardData.GetCardGroup(cardType);
                if (string.IsNullOrEmpty(cardGroup)) CommonFn.ShowToast(ToastrControl.toastrError, "ไม่พบ card group");
                Debug.Log($"cardGroup = {cardGroup}");
                string license = "";
                //string cardNumber = CardData.GetCardNumber(uid);
                byte[] readData = CardData.ReadSector(Constant.infoSector);
                if (cardGroup == Constant.CardGroup.visitor)
                {
                    if (licenseCodeInput.text == "")
                    {
                        CommonFn.ShowToast(ToastrControl.toastrError, "ใส่ทะเบียนก่อนทำรายการ");
                        return;
                    }

                    if (readData.Length == 0)
                    {
                        Debug.LogError("read sector 8 block 2 fail");
                        CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
                        return;
                    }

                    if (!CardData.IsCardEmpty(readData))
                    {
                        CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการออก");
                        return;
                    }

                    license = licensePrefixInput.text + licenseCodeInput.text;
                    license = license.PadRight(8, '|');
                }
                else if (cardGroup == Constant.CardGroup.member)
                {
                    var enterGateId = Config.Get(Constant.enterGateId);
                    //Debug.Log($"enterGateId = {enterGateId}");
                    if (!Util.IsAllowAccessGate(uid, enterGateId))
                    {
                        CommonFn.ShowToast(ToastrControl.toastrError, $"ไม่อนุญาติให้ผ่านประตู Id {enterGateId}");
                        return;
                    }

                    license = CardData.GetLicense(uid);
                    license = license.PadRight(8, '|');
                    //Debug.Log($"license = {license}");
                    if (readData.Length == 0)
                    {
                        EnterGateMemberShareSite(license, uid);
                        return;
                    }
                    else
                    {
                        if (!CardData.IsCardEmpty(readData))
                        {
                            CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการออก");
                            return;
                        }
                    }
                }
                else
                {
                    CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่อยู่ในระบบ");
                    return;
                }

                EnterGate(readData, license, uid);
            }
            else
            {
                Debug.Log("card is not mifare classic");
                CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเขียนบัตร");
        }
    }
    public void JavaSendMessage(string result)
    {
        try
        {
            Debug.Log($"result java = {result}");
            //if (result.Contains("TAG_DISCOVERED"))
            //{
            //    string uid = CardData.GetUID();
            //    //var uid = "FA2A9B26";
            //    Debug.Log($"uid = {uid}");
            //    if (string.IsNullOrEmpty(uid)) CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่าน UID ของบัตรได้");
            //    bool IsCardActive = CardData.IsCardActive(uid);
            //    if(!IsCardActive) CommonFn.ShowToast(ToastrControl.toastrError, "บัตรถูกระงับการใช้งาน");
            //    string cardType = CardData.GetCardType(uid);
            //    if (string.IsNullOrEmpty(cardType)) CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่อยู่ในระบบ");
            //    Debug.Log($"cardType = {cardType}");
            //    string cardGroup = CardData.GetCardGroup(cardType);
            //    if (string.IsNullOrEmpty(cardGroup)) CommonFn.ShowToast(ToastrControl.toastrError, "ไม่พบ card group");
            //    Debug.Log($"cardGroup = {cardGroup}");
            //    string license = "";
            //    //string cardNumber = CardData.GetCardNumber(uid);
            //    byte[] readData = CardData.ReadSector(Constant.infoSector);
            //    if (cardGroup == Constant.CardGroup.visitor)
            //    {
            //        if (licenseCodeInput.text == "")
            //        {
            //            CommonFn.ShowToast(ToastrControl.toastrError, "ใส่ทะเบียนก่อนทำรายการ");
            //            return;
            //        }

            //        if (readData.Length == 0)
            //        {
            //            Debug.LogError("read sector 8 block 2 fail");
            //            CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
            //            return;
            //        }

            //        if (!CardData.IsCardEmpty(readData))
            //        {
            //            CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการออก");
            //            return;
            //        }

            //        license = licensePrefixInput.text + licenseCodeInput.text;
            //        license = license.PadRight(8, '|');
            //    }
            //    else if(cardGroup == Constant.CardGroup.member)
            //    {
            //        var enterGateId = Config.Get(Constant.enterGateId);
            //        //Debug.Log($"enterGateId = {enterGateId}");
            //        if(!Util.IsAllowAccessGate(uid, enterGateId))
            //        {
            //            CommonFn.ShowToast(ToastrControl.toastrError, $"ไม่อนุญาติให้ผ่านประตู Id {enterGateId}");
            //            return;
            //        }

            //        license = CardData.GetLicense(uid);
            //        license = license.PadRight(8, '|');
            //        //Debug.Log($"license = {license}");
            //        if (readData.Length == 0)
            //        {
            //            EnterGateMemberShareSite(license, uid);
            //            return;
            //        }
            //        else
            //        {
            //            if (!CardData.IsCardEmpty(readData))
            //            {
            //                CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการออก");
            //                return;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่อยู่ในระบบ");
            //        return;
            //    }

            //    EnterGate(readData, license, uid);
            //}
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเขียนบัตร");
        }
    }

    public void sendMsg(string msg)
    {
        Debug.Log("send from java " + msg);
    }

    private void EnterGate(byte[] inputData, string license, string uid)
    {
        try
        {
            CardData.ByteFormat cardInfo = cardInfo = CardData.CreateCardInfo(inputData);
            Transaction.Enter[] transactions = PrepareTransaction(cardInfo.cardId, license);
            string[] writeData = PrepareWriteTagData(inputData, license);
            Debug.Log(writeData[0]);
            Debug.Log(writeData[1]);
            if (this.WriteTags(Constant.infoSector, 1, writeData[0]) && 
                this.WriteTags(Constant.infoSector, 2, writeData[1]))
            {
                licenseCodeInput.text = "";
                licensePrefixInput.text = "";
                Transaction.Send(transactions, Constant.EnterGate);
                string cardNumber = CardData.GetCardNumber(uid);
                CommonFn.ShowToast(ToastrControl.toastrSuccess, $"บัตร {cardNumber} ทำรายการขาเข้าสำเร็จ");
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "เขียนบัตรไม่สำเร็จโปรดลองใหม่อีกครั้ง");
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเขียนบัตร");
        }
    }

    private void EnterGateMemberShareSite(string license, string uid)
    {
        try
        {
            byte[] uidBytes = Util.ToByteArray(uid);
            var cardInfo = new CardData.ByteFormat() { cardId = uidBytes };
            Transaction.Enter[] transactions = PrepareTransaction(cardInfo.cardId, license);
            Transaction.Send(transactions, Constant.EnterGate);
            string cardNumber = CardData.GetCardNumber(uid);
            CommonFn.ShowToast(ToastrControl.toastrSuccess, $"บัตร {cardNumber} ทำรายการขาเข้าสำเร็จ");
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเขียนบัตร");
        }
    }

    private bool WriteTags(int sector, int blockIndex, string data)
    {
        //CommonFn.callLoading(true);
        try
        {
            // blockIndex start from 0
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            //string dummyData = "F1E2D3C4B5A60708090A0B0C0D0E0F33";
            int result = ajc.CallStatic<int>("writeTagClick", blockIndex, sector, data, Constant.passINNET);
            //Debug.Log("WriteTagProcess = " + result);
            if(result == 0)
            {
                return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private string[] PrepareWriteTagData(byte[] readData, string license)
    {
        try
        {
            //Parking Info(26 Bytes)
            //Sector: 8
            //Start Address : 16
            //Password: “INNET9”	Card Status(1 Byte)
            //Stamp Counter(1 Byte)
            //Gate ID( 1 Byte)
            //Province ID(1 Bytes)
            //License(16 Bytes)
            //Time Stamp(6 Bytes)
            //Payment Counter(1 Byte)
            //Password(4 Byte)

            string[] result = new string[2];
            //TODO toArray ทำให้ reference มันขาดจริงไหม
            byte[] writeData = readData.ToArray();
            Debug.Log("writeData Length = " + writeData.Length);

            //Status
            writeData[16] = 1;
            //Stamp Counter
            writeData[17] = 0;
            //Gate ID
            writeData[18] = byte.Parse(Config.Get(Constant.enterGateId));
            //Province Counter
            writeData[19] = 0;

            //Debug.Log($"PrepareWriteTagData : license = {license}");
           
            byte[] licenseBytes = System.Text.UnicodeEncoding.BigEndianUnicode.GetBytes(license);
            //Debug.Log($"unicode len = {licenseBytes.Length}");

            for (int i = 0; i < licenseBytes.Length; i++)
            {
                writeData[20 + i] = licenseBytes[i];
            }

            //Time
            int yearInt = DateTime.Now.Year - 2000;
            string year = yearInt.ToString();
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string min = DateTime.Now.Minute.ToString();
            string sec = DateTime.Now.Second.ToString();

            writeData[36] = byte.Parse(year);
            //Debug.Log($"year = {year}");
            writeData[37] = byte.Parse(month);
            //Debug.Log($"month = {month}");
            writeData[38] = byte.Parse(day);
            //Debug.Log($"day = {day}");
            writeData[39] = byte.Parse(hour);
            //Debug.Log($"hour = {hour}");
            writeData[40] = byte.Parse(min);
            //Debug.Log($"min = {min}");
            writeData[41] = byte.Parse(sec);
            //Debug.Log($"sec = {sec}");

            //write block 2, 3
            result[0] = Util.ByteArrayToHexStringWithSelectBlock(writeData, 1);
            result[1] = Util.ByteArrayToHexStringWithSelectBlock(writeData, 2);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private Transaction.Enter[] PrepareTransaction(byte[] cardNumber, string license)
    {
        try
        {
            //Parking Info(26 Bytes)
            //Sector: 8
            //Start Address : 16
            //Password: “INNET9”	Card Status(1 Byte)
            //Stamp Counter(1 Byte)
            //Gate ID( 1 Byte)
            //Province ID(1 Bytes)
            //License(16 Bytes)
            //Time Stamp(6 Bytes)
            //Payment Counter(1 Byte)
            //Password(4 Byte)

            var enterTransactions = new Transaction.Enter[1] 
            {
                new Transaction.Enter
                {
                    enterDatetime = DateTime.Now,
                    gateId = int.Parse(Config.Get(Constant.enterGateId)),
                    license = license,
                    cardNumber = (uint)((cardNumber[0] << 32)
                          | (cardNumber[1] << 16)
                          | (cardNumber[2] << 8)
                          | (cardNumber[3])
                          ),
                    userId = int.Parse(Config.Get(Constant.activeUserId)),
                    shiftId = Util.GetShiftId()
                }
            };

            return enterTransactions;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
