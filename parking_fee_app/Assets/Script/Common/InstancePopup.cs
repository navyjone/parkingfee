using System.Collections;
using UnityEngine;

public class InstancePopup : MonoBehaviour {
    
    private void Update() {
        if (this.gameObject.activeSelf)
        {
            StartCoroutine(this.DelayInactive(3));
            // TODO: animation
        }
    }

    private IEnumerator DelayInactive (int second)
    {
        yield return new WaitForSeconds(second);
        this.gameObject.SetActive(false);
    }
}