﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Data;
using RestSharp;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Text.RegularExpressions;

public class MainEventExitGate : MonoBehaviour {

    public GameObject PrintImg;
    public GameObject StatusIcon;
    public GameObject ScanCard;
    public GameObject PaymentDetail;
    public GameObject Summary;
    public GameObject NextBt;
    public GameObject PrintBt;
    public GameObject PayBt;
    public GameObject CancelBt;
    public GameObject ReprintBt;
    public GameObject PaymentPanel;
    public GameObject SuccessPanel;
    public GameObject ReadFailPanel;
    public GameObject DiscountPanel;
    public GameObject DiscountScrollViewContent;
    public GameObject DiscountBt;
    public GameObject PrinterStatusIcon;
    public Text UserTx;

    System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
    public delegate bool AsyncMethodCaller();
    private AndroidJavaObject activityContext = null;

    void Start ()
    {
        try
        {
            Initial();
            Config.LoadXmlToDataTable();
            stopwatch.Start();

            if (EStampManual.IsDisplay() == Constant.TRUE)
            {
                DiscountBt.SetActive(true);
                EStampManual.CreateDiscountRow(DiscountScrollViewContent);
            }
            else
            {
                DiscountBt.SetActive(false);
            }

            //CommonFn.ShowLoading(true);
            //PrinterBluetooth.GetStatusDeletgate(Config.Get(Constant.activePrinter), Constant.ExitGate, this.activityContext);
            UserTx.text = "ผู้ใช้งานระบบ: " + ShareInstance.userName;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเริ่มต้นทำงาน");
        }
    }

    public void Initial()
    {
        UserTx.text = "ผู้ใช้งานระบบ: " + ShareInstance.userName;
        PrintImg.SetActive(true);
        StatusIcon.SetActive(true);
        ScanCard.SetActive(true);
        ScanCard.transform.Find("CarImg").gameObject.SetActive(false);
        PaymentDetail.SetActive(false);
        Summary.SetActive(false);
        NextBt.SetActive(false);
        PrintBt.SetActive(false);
        PayBt.SetActive(false);
        CancelBt.SetActive(false);
        ReprintBt.SetActive(false);
        PaymentPanel.SetActive(false);
        SuccessPanel.SetActive(false);
        ReadFailPanel.SetActive(false);
        DiscountPanel.SetActive(false);
        PrinterZKC.Init();
    }

    public void BackBtClick()
    {
        SceneManager.LoadScene(Constant.SelectMode);
    }

    // Update is called once per frame
    void Update ()
    {
        if (stopwatch.Elapsed.TotalSeconds >= 5)
        {
            stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            Transaction.ResendFailTransaction();
        }

        if (Application.platform == RuntimePlatform.Android)
        {

            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {

                // Quit the application
                Application.Quit();
            }
        }
    }

    #region read card

    //Event when card is read
    public void OnNewIntentEvent(string result)
    {
        if (result == Constant.mifareClassic)
        {
            try
            {
                CardData cardData = GetCardData();
                if (cardData != null && !Summary.activeSelf)
                {
                    ScanCard.SetActive(false);
                    PaymentDetail.SetActive(true);
                    ((Payment)PaymentDetail.GetComponent(typeof(Payment))).Initial(cardData);
                }
                else
                {
                    ScanCard.SetActive(true);
                    PaymentDetail.SetActive(false);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
                CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
            }
        }
        else
        {
            Debug.Log("card is not mifare classic");
            CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
        }
    }

    public void JavaSendMessage(string result)
    {
        Debug.Log("unity send msg :" + result);
        //if (result.Contains("reader"))
        //{
        //    Debug.Log("unity send msg :" + result);
        //}

        //if (result.Contains("TAG_DISCOVERED"))
        //{
        //    try
        //    {
        //        CardData cardData = GetCardData();
        //        if(cardData != null && !Summary.activeSelf)
        //        {
        //            ScanCard.SetActive(false);
        //            PaymentDetail.SetActive(true);
        //            ((Payment)PaymentDetail.GetComponent(typeof(Payment))).Initial(cardData);
        //        }
        //        else
        //        {
        //            ScanCard.SetActive(true);
        //            PaymentDetail.SetActive(false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.LogError(ex.ToString());
        //        CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
        //    }
        //}
    }

    public void sendMsg(string msg)
    {
        Debug.Log("send from java " + msg);
    }

    //public void ReadDataEvent(string msg)
    //{
    //    //Debug.Log("msgJava=" + msg);
    //    try
    //    {
    //        if (!msg.Contains("read error"))
    //        {
    //            CardData cardData = CardData.ReadFromString(msg);
    //            if (cardData == null)
    //            {
    //                ScanCard.SetActive(true);
    //                PaymentDetail.SetActive(false);
    //                return;
    //            }
    //            //Debug.Log("licensePrefix = " + cardData.licensePrefix);
    //            //Debug.Log("licenseCode = " + cardData.licenseCode);
    //            //Debug.Log("enterDate = " + cardData.enterDate);
    //            //Debug.Log("exitDate = " + cardData.exitDate);
    //            //Debug.Log("enterTime = " + cardData.enterTime);
    //            //Debug.Log("exitTime = " + cardData.exitTime);
    //            //Debug.Log("stampCode = " + cardData.stampCode);
    //            //Debug.Log("parkingTotalTime = " + cardData.parkingTotalTime);
    //            //Debug.Log("fee = " + cardData.fee);
    //            //Debug.Log("discount = " + cardData.discount);
    //            //Debug.Log("Summary.activeSelf = " + Summary.activeSelf);
    //            if (!Summary.activeSelf)
    //            {
    //                ScanCard.SetActive(false);
    //                PaymentDetail.SetActive(true);
    //                ((Payment)PaymentDetail.GetComponent(typeof(Payment))).Initial(cardData);
    //            }
    //        }
    //        else
    //        {
    //            ScanCard.SetActive(true);
    //            PaymentDetail.SetActive(false);
    //            CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Debug.LogError(ex.ToString());
    //        ScanCard.SetActive(true);
    //        PaymentDetail.SetActive(false);
    //        CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
    //    }
    //}

    private CardData PrepareDataExitGate(byte[] readData, string cardNumber)
    {
        try
        {
            CardData.ByteFormat cardInfo = CardData.CreateCardInfo(readData);
            DateTime enterDateTime = new DateTime();
            string[] parkingTime = FindParkingTotalTime(cardInfo, out enterDateTime);
            string[] licensePrefixAndCode = GetLicencse(cardInfo.license);
            CardData response = new CardData
            {
                gateId = cardInfo.gateId,
                parkingHour = parkingTime[0],
                parkingMinute = parkingTime[1],
                enterDateTime = enterDateTime,
                exitDateTime = DateTime.Now,
                //enterDate = enterDateTime.Day.ToString("D2") + "/" + enterDateTime.Month.ToString("D2") + "/" + enterDateTime.Year.ToString(),
                //enterTime = enterDateTime.Hour.ToString("D2") + ":" + enterDateTime.Minute.ToString("D2"),
                //exitDate = DateTime.Now.Day.ToString("D2") + "/" + DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year.ToString(),
                //exitTime = DateTime.Now.Hour.ToString("D2") + ":" + DateTime.Now.Minute.ToString("D2"),
                licensePrefix = licensePrefixAndCode[0],
                licenseCode = licensePrefixAndCode[1],
                cardId = (uint)((cardInfo.cardId[0] << 32)
                                | (cardInfo.cardId[1] << 16)
                                | (cardInfo.cardId[2] << 8)
                                | (cardInfo.cardId[3])
                                ),
                cardTypeId = cardInfo.cardType,
                cardNumber = cardNumber,
            };

            int[] stampIds = GetStampIds(cardInfo.stampCounter);
            string stampCode = "";
            CalFee.FeeInfo fee = null;
            //there is a case that stampcode deduct fee and discount is 0
            if (stampIds.Length > 0)
            {
                stampCode = FindBestStampCode(enterDateTime, cardInfo.cardType, stampIds);
                response.stampCode = stampCode;
                response.stampId = GetStampId(stampCode);
                fee = FindFeeFromStampDiscount(stampCode, enterDateTime, cardInfo.cardType);
            }
            else
            {
                Debug.Log($"cardInfo.cardType = {cardInfo.cardType}");
                fee = CalFee.CalculateParkingFee(enterDateTime, DateTime.Now, "", 0, cardInfo.cardType, 0, 0);
            }

            // check customer already paid or not
            response.fee = GetAmountPaid(cardInfo.paidCounter, fee);
            response.discount = fee.discount_baht;
            response.net = response.fee - response.discount;
            Debug.Log("Discount = " + response.discount.ToString());
            Debug.Log("Total fee = " + response.fee.ToString());
            Debug.Log("net = " + response.net.ToString());
            return response;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private CardData PrepareDataExitGateMemberShareSite(string uid, string license, string cardNumber, string cardTypeId)
    {
        try
        {
            byte[] uidBytes = Util.ToByteArray(uid);
            string[] licenses = GetLicencse(license);
            CardData response = new CardData
            {
                cardId = (uint)((uidBytes[0] << 32)
                                | (uidBytes[1] << 16)
                                | (uidBytes[2] << 8)
                                | (uidBytes[3])
                                ),
                cardTypeId = int.Parse(cardTypeId),
                cardNumber = cardNumber,
                licensePrefix = licenses[0],
                licenseCode = licenses[1],
            };

            return response;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    private CardData GetCardData()
    {
        try
        {
            var cardDataResult = new CardData();
            string uid = CardData.GetUID();
            Debug.Log($"read exit uid = {uid}");
            if (string.IsNullOrEmpty(uid)) CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่าน UID ของบัตรได้");
            string cardType = CardData.GetCardType(uid);
            if (string.IsNullOrEmpty(cardType)) CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่อยู่ในระบบ");
            string cardGroup = CardData.GetCardGroup(cardType);
            if (string.IsNullOrEmpty(cardGroup)) CommonFn.ShowToast(ToastrControl.toastrError, "ไม่พบ card group");

            string cardNumber = CardData.GetCardNumber(uid);
            byte[] readData = CardData.ReadSector(Constant.infoSector);
            if (cardGroup == Constant.CardGroup.visitor)
            {
                if (readData.Length == 0)
                {
                    Debug.LogError("read sector 8 block 2 fail");
                    CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถอ่านข้อมูลในบัตรได้");
                    return null;
                }

                if (CardData.IsCardEmpty(readData))
                {
                    CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการเข้า");
                    return null;
                }
            }
            else if (cardGroup == Constant.CardGroup.member)
            {
                var enterGateId = Config.Get(Constant.enterGateId);
                if (!Util.IsAllowAccessGate(uid, enterGateId))
                {
                    CommonFn.ShowToast(ToastrControl.toastrError, $"ไม่อนุญาติให้ผ่านประตู Id {enterGateId}");
                    return null;
                }

                string license = CardData.GetLicense(uid);
                if (readData.Length == 0)
                {
                    cardDataResult = PrepareDataExitGateMemberShareSite(uid, license, cardNumber, cardType);
                    return cardDataResult;
                }
                else
                {
                    if (CardData.IsCardEmpty(readData))
                    {
                        CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการเข้า");
                        return null;
                    }
                }
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่อยู่ในระบบ");
                return null;
            }

            cardDataResult = PrepareDataExitGate(readData, cardNumber);
            return cardDataResult;            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //obsolete
    public void ChangeColorPrinterStatusIcon(bool isConnect)
    {
        CommonFn.ShowLoading(false);
        //if (isConnect)
        //{
        //    PrinterStatusIcon.GetComponent<Image>().color = Color.green;
        //}
        //else
        //{
        //    PrinterStatusIcon.GetComponent<Image>().color = Color.red;
        //    Debug.Log("can't connect printer");
        //}
    }

    public void PrintResultCallBack(bool isSuccess)
    {
        try
        {
            CommonFn.ShowLoading(false);
            ChangeColorPrinterStatusIcon(isSuccess);
            if (isSuccess)
            {
                CommonFn.ShowToast(ToastrControl.toastrSuccess, "พิมพ์ใบเสร็จรับเงินสำเร็จ");
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "พิมพ์ใบเสร็จรับเงินล้มเหลว");
            }
        }
        catch(Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการพิมพ์ใบเสร็จรับเงิน");
            Debug.LogError(ex.ToString());
        }
    }

    private static string[] FindParkingTotalTime(CardData.ByteFormat card, out DateTime enterDateTime)
    {
        //int year = card.startTime[2] + 2000;
        //string enterTime = year.ToString() + "-" + card.startTime[1].ToString("D2") + "-" + card.startTime[0].ToString("D2") + " " +
        //                   card.startTime[3].ToString("D2") + ":" + card.startTime[4].ToString("D2");
        int year = card.startTime[0] + 2000;
        string enterTime = year.ToString() + "-" + card.startTime[1].ToString("D2") + "-" + card.startTime[2].ToString("D2") + " " +
                    card.startTime[3].ToString("D2") + ":" + card.startTime[4].ToString("D2");
        //Debug.Log("enterTime convert = " + enterTime);
        enterDateTime = DateTime.Parse(enterTime);
        TimeSpan timespan = DateTime.Now - enterDateTime;
        double totalSec = timespan.TotalSeconds;
        string[] result = new string[2];
        result[0] = string.Format("{0:00}", totalSec / 3600);
        result[1] = string.Format("{0:00}", (totalSec / 60) % 60);
        //string parkingTotalTime = string.Format("{0:00} ชั่วโมง {1:00} นาที", totalSec / 3600, (totalSec / 60) % 60);
        return result;
    }

    private static int GetAmountPaid(int totalPaidCounter, CalFee.FeeInfo fee)
    {
        #region read paid counter
        int MAX_PAID = 2;
        //int totalPaidCounter = cardData[42];
        //int totalPaidCounter = paidCounter;
        int totalSectorPayCounter = 0;
        int[] nameSectorPayCounter = new int[] { 1, 2 };
        string[] dataReadPaidCounterStrArr = new string[1];
        uint[] valuePaidArr = new uint[totalPaidCounter];
        uint[] exitTimeArr = new uint[totalPaidCounter];
        DateTime[] dateTimePaidCounter = new DateTime[totalPaidCounter];
        int indexPayCounter = 0;
        string day = "";
        string mon = "";
        int yearInt = 0;
        string yearStr = "";
        string hour = "";
        string min = "";
        string sec = "";
        //Debug.Log("total paid: " + totalPaidCounter);
        //Debug.Log("Total Stamp = " + totalStamp);
        if (totalPaidCounter > 0)
        {
            totalSectorPayCounter = totalPaidCounter / 3;
            //Debug.Log("total paycounter: " + totalSectorPayCounter);
            if ((totalPaidCounter % 3) > 0)
            {
                totalSectorPayCounter++;
            }
            //Debug.Log("totalStamp = " + totalStamp + " totalSectorStamp = " + totalSectorStamp);
            for (int i = 0; i < totalSectorPayCounter; i++)
            {
                try
                {
                    AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
                    dataReadPaidCounterStrArr = ajc.CallStatic<string[]>("ReadTagReturnStringArray", nameSectorPayCounter[i], Constant.passINNET);
                    byte[] payCounterByteBuffer = Util.ToByteArray(dataReadPaidCounterStrArr[0] + dataReadPaidCounterStrArr[1] + dataReadPaidCounterStrArr[2] + dataReadPaidCounterStrArr[3]);
                    for (int j = 0; j < 3; j++)
                    {
                        if (totalPaidCounter > 0)
                        {
                            day = payCounterByteBuffer[(j * 14) + 2].ToString("D2");
                            mon = payCounterByteBuffer[(j * 14) + 3].ToString("D2");
                            yearInt = payCounterByteBuffer[(j * 14) + 4] + 2000;
                            yearStr = yearInt.ToString("D4");
                            hour = payCounterByteBuffer[(j * 14) + 5].ToString("D2");
                            min = payCounterByteBuffer[(j * 14) + 6].ToString("D2");
                            sec = payCounterByteBuffer[(j * 14) + 7].ToString("D2");
                            bool result = DateTime.TryParse(yearStr + "-" + mon + "-" + day + " " + hour + ":" + min + ":" + sec, out dateTimePaidCounter[indexPayCounter]);
                            //Debug.Log("parse time : " + result);
                            if (!result)
                            {
                                Debug.Log("DateTime.TryParse error " + yearStr + "-" + mon + "-" + day + " " + hour + ":" + min + ":" + sec);
                            }

                            valuePaidArr[indexPayCounter] = (uint)((payCounterByteBuffer[(j * 14) + 8] << 32)
                                | (payCounterByteBuffer[(j * 14) + 9] << 16)
                                | (payCounterByteBuffer[(j * 14) + 10] << 8)
                                | (payCounterByteBuffer[(j * 14) + 11])
                                );

                            exitTimeArr[indexPayCounter] = (uint)((payCounterByteBuffer[(j * 14) + 12] << 8)
                            | (payCounterByteBuffer[(j * 14) + 13])
                            );
                        }
                        indexPayCounter++;
                        totalPaidCounter--;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e);
                }
                //Debug.Log("name sector = " + nameSectorStamp[i]);
                //for (int k = 0; k < dataReadStampStrArr.Length; k++)
                //{
                //    Debug.Log("dataReadStampStrArr index = " + k + " val = " + dataReadStampStrArr[k]);
                //}
            }

            //for (int k = 0; k < valuePaidArr.Length; k++)
            //{
            //    Debug.Log("payCounterIntArr index = " + k + " val = " + valuePaidArr[k]);
            //}
        }

        int PAYMENT_AMOUNT = 0;
        int will_pay_value = 0;
        if (valuePaidArr.Length >= MAX_PAID)
        {
            Debug.Log("Can not pay by this machine due to this card has paid = " + valuePaidArr.Length + " times already.");
            return 0;
        }
        else
        {
            uint paid_value = 0;
            for (int i = 0; i < valuePaidArr.Length; i++)
            {
                paid_value += valuePaidArr[i];
            }

            //Debug.Log("Sum of paid = " + paid_value);

            if ((uint)fee.TotalFee > paid_value)
            {
                will_pay_value = (int)(fee.TotalFee - paid_value);
                //Debug.Log("Have to pay more = " + will_pay_value);
                // TODO: PAYMENT_AMOUNT ??
                PAYMENT_AMOUNT = will_pay_value;
            }
        }

        return will_pay_value;
        #endregion
    }

    //private static string[] GetLicencse(byte[] cardData)
    //{
    //    string license = "";
    //    int licenseAddress = 20;
    //    for (int i = 0; i < 16; i = i + 2)
    //    {
    //        byte[] data = new byte[] { cardData[licenseAddress], cardData[licenseAddress + 1] };
    //        string hex = ByteArrayToHexStringLicense(data);
    //        license += System.Text.RegularExpressions.Regex.Unescape("\\u" + hex);
    //        if (i == 6)
    //        {
    //            license += "-";
    //        }
    //        licenseAddress += 2;
    //    }

    //    string[] licenseSplits = license.Split('-');
    //    return licenseSplits;
    //}

    //private static string ByteArrayToHexStringLicense(byte[] data)
    //{
    //    StringBuilder sb = new StringBuilder(data.Length * 3);
    //    foreach (byte b in data)
    //        sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
    //    return sb.ToString().ToUpper();
    //}

    private static string FindBestStampCode(DateTime enterDateTime, byte cardType, int[] stampIds)
    {
        string result = "";
        CalFee.FeeInfo fee = new CalFee.FeeInfo();
        Dictionary<string, int> stampDict = new Dictionary<string, int>();
        if (stampIds.Length > 0)
        {
            for (int i = 0; i < stampIds.Length; i++)
            {
                //string stampCode = GetStampCode(stampIds[i], cardType, 0);
                string stampCode = stampIds[i].ToString("D5");
                if (!stampDict.ContainsKey(stampCode))
                {
                    fee = CalFee.CalculateParkingFee(enterDateTime, DateTime.Now, stampCode, 0, cardType, 0, 0);
                    stampDict.Add(stampCode, fee.discount_baht);
                }
            }

            //find best discount
            var stampDictSorted = (from entry in stampDict
                                   orderby entry.Value
                                   descending
                                   select entry).ToDictionary
                          (
                           pair => pair.Key,
                           pair => pair.Value
                          ).Take(1);
            result = stampDictSorted.ElementAt(0).Key;
        }

        return result;
    }

    private static string[] GetLicencse(byte[] licenseByte)
    {
        try
        {
            //byte[] licenseByte = SubArrayDeepClone(license, 20, 16);
            //parkingInfo.LicenseNumber = System.Text.UnicodeEncoding.BigEndianUnicode.GetString(license).Replace("|", "");
            //string license = System.Text.Encoding.Unicode.GetString(licenseByte).Replace("|", "");
            string license = System.Text.UnicodeEncoding.BigEndianUnicode.GetString(licenseByte).Replace("|", "");
            Debug.Log("license = " + license);
            //How Regex.Split work 
            //example 3กข1235 result[0] = 3, result[1] = 1235
            //example กข1234 reuslt[0] = "", result[1] = 1234
            string[] licenseSplits = Regex.Split(license, @"\D+");
            Debug.Log("licenseSplits.lenght=" + licenseSplits.Length);
            string[] result = new string[2];
            //Debug.Log("licenseSplits[0]=" + licenseSplits[0]);
            //Debug.Log("licenseSplits[1]=" + licenseSplits[1]);
            if (licenseSplits.Length > 1)
            {
                result[0] = license.Replace(licenseSplits[1], "");
                result[1] = licenseSplits[1];
            }
            else
            {
                result[0] = "";
                result[1] = licenseSplits[0];
            }

            //Debug.Log("result[0]=" + result[0]);
            //Debug.Log("result[0]=" + result[1]);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            string[] result = new string[2];
            return result;
        }
    }

    private static string[] GetLicencse(string license)
    {
        try
        {
            //byte[] licenseByte = SubArrayDeepClone(license, 20, 16);
            //parkingInfo.LicenseNumber = System.Text.UnicodeEncoding.BigEndianUnicode.GetString(license).Replace("|", "");
            //string license = System.Text.Encoding.Unicode.GetString(licenseByte).Replace("|", "");
            Debug.Log("license = " + license);
            //How Regex.Split work 
            //example 3กข1235 result[0] = 3, result[1] = 1235
            //example กข1234 reuslt[0] = "", result[1] = 1234
            string[] licenseSplits = Regex.Split(license, @"\D+");
            Debug.Log("licenseSplits.lenght=" + licenseSplits.Length);
            string[] result = new string[2];
            //Debug.Log("licenseSplits[0]=" + licenseSplits[0]);
            //Debug.Log("licenseSplits[1]=" + licenseSplits[1]);
            if (licenseSplits.Length > 1)
            {
                result[0] = license.Replace(licenseSplits[1], "");
                result[1] = licenseSplits[1];
            }
            else
            {
                result[0] = "";
                result[1] = licenseSplits[0];
            }

            //Debug.Log("result[0]=" + result[0]);
            //Debug.Log("result[0]=" + result[1]);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            string[] result = new string[2];
            return result;
        }
    }

    private static CalFee.FeeInfo FindFeeFromStampDiscount(string stampCode, DateTime enterDateTime, byte cardType)
    {
        CalFee.FeeInfo feeResult = CalFee.CalculateParkingFee(enterDateTime, DateTime.Now, stampCode, 0, cardType, 0, 0);
        CalFee.FeeInfo feeBeforeDiscount = CalFee.CalculateParkingFee(enterDateTime, DateTime.Now, "", 0, cardType, 0, 0);
        //feeResult.discount = newFee.discount_baht;
        Debug.Log("fee before discount = " + feeBeforeDiscount.ParkingFee);
        Debug.Log("fee after discount = " + feeResult.ParkingFee);

        if (feeBeforeDiscount.ParkingFee > feeResult.ParkingFee)
        {
            feeResult.discount_baht = (feeBeforeDiscount.ParkingFee - feeResult.ParkingFee) + feeResult.discount_baht;
            feeResult.TotalFee = feeBeforeDiscount.ParkingFee;
        }

        Debug.Log("feeResult discount = " + feeResult.discount_baht);

        return feeResult;
    }

    private static int GetStampId(string name)
    {
        int stampId = 0;
        DataRow[] row = Config.dtStampInfo.Select("STAMP_CODE = '" + name + "' and active_id = 'Y'");
        if (row.Length > 0)
        {
            stampId = int.Parse(row[0]["STAMP_ID"].ToString());
        }

        return stampId;
    }

    private static int[] GetStampIds(byte stampCounter)
    {
        #region list stamp counter
        int totalStamp = stampCounter;
        int totalSectorStamp = 0;
        int[] nameSectorStamp = new int[] { 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15 };
        string[] dataReadStampStrArr = new string[1];
        int[] stampIds = new int[totalStamp];
        int indexStamp = 0;
        if (totalStamp > 0)
        {
            totalSectorStamp = totalStamp / 4;
            if ((totalStamp % 4) > 0)
            {
                totalSectorStamp++;
            }

            Debug.Log("totalStamp = " + totalStamp + " totalSectorStamp = " + totalSectorStamp);
            for (int i = 0; i < totalSectorStamp; i++)
            {
                try
                {
                    AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
                    dataReadStampStrArr = ajc.CallStatic<string[]>("ReadTagReturnStringArray", nameSectorStamp[i], Constant.passINNET);
                    byte[] stampByteBuffer = Util.ToByteArray(dataReadStampStrArr[0] + dataReadStampStrArr[1] + dataReadStampStrArr[2] + dataReadStampStrArr[3]);
                    for (int j = 0; j < 4; j++)
                    {
                        if (totalStamp > 0)
                        {
                            stampIds[indexStamp++] = (int)(stampByteBuffer[(j * 12) + 10] << 8) | stampByteBuffer[(j * 12) + 11];
                        }
                        totalStamp--;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e);
                }
                //Debug.Log("name sector = " + nameSectorStamp[i]);
                //for (int k = 0; k < dataReadStampStrArr.Length; k++)
                //{
                //    Debug.Log("dataReadStampStrArr index = " + k + " val = " + dataReadStampStrArr[k]);
                //}
            }

            for (int k = 0; k < stampIds.Length; k++)
            {
                Debug.Log("stampIntArr index = " + k + " val = " + stampIds[k]);
            }
        }

        #endregion
        return stampIds;
    }

}
