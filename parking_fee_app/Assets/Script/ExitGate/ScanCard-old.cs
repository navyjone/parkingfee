using UnityEngine;

public class ScanCard : MonoBehaviour {
    public int TempChange;
    public GameObject CarImg;
    public GameObject NextBt;
    public GameObject FailPanel;
    public GameObject PaymentObj;

    private void Update() 
    {
        if (TempChange == 1)
        {
            this.Read();
        }
        else if (TempChange == 2)
        {
            this.Fail();
        }
        else
        {
            Initial();
        }
    }

    public void Initial () 
    {
        CarImg.SetActive(false);
        FailPanel.SetActive(false);
        NextBt.SetActive(false);
    }

    private void Read () 
    {
        CarImg.SetActive(true);
        NextBt.SetActive(true);
    }

    private void Fail () 
    {
        FailPanel.SetActive(true);
    }

    //public void NextBtClick () 
    //{
    //    this.gameObject.SetActive(false);
    //    PaymentObj.SetActive(true);
    //    var card = new CardData();
    //    card.licensePrefix = "00";
    //    card.licenseCode = "7894";
    //    card.enterDate = "18/07/2018";
    //    card.enterTime = "18:00";
    //    card.exitTime = "20:00";
    //    card.fee = 90;
    //    card.discount = 0;
    //    card.net = 90;
    //    ((Payment)PaymentObj.GetComponent(typeof(Payment))).Initial(card);
    //}
}