﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

[System.Serializable]
public class CardData
{
    public string licensePrefix;
    public string licenseCode;
    //public string enterDate;
    //public string enterTime;
    //public string exitDate;
    //public string exitTime;
    public DateTime enterDateTime;
    public DateTime exitDateTime;
    public int fee;
    public int discount;
    public int net;
    public int paid;
    public int change;
    public int stampId;
    public string stampCode;
    public string parkingHour;
    public string parkingMinute;
    public int cardTypeId;
    public uint cardId;
    public int gateId;
    public string cardNumber;

    public struct ByteFormat
    {
        public byte[] cardId;
        public byte cardType;
        public byte cardStatus;
        public byte stampCounter;
        public byte gateId;
        public byte provinceId;
        public byte[] license;
        public byte[] startTime;
        public byte paidCounter;
        public byte[] password;
        public byte stampId;
    }

    //public static CardData ReadFromString(string inputString)
    //{
    //    try
    //    {
    //        CardData response = new CardData();
    //        byte[] inputBytes = ConvertStringToByte(inputString);
    //        CardData.ByteFormat cardInfo = CreateCardInfo(inputBytes);

    //        if (!CardData.IsCardIdInSystem(cardInfo.cardId))
    //        {
    //            CommonFn.ShowToast(ToastrControl.toastrError, "บัตรไม่ได้อยู่ในระบบ");
    //            return null;
    //        }

    //        if ((cardInfo.startTime[0] == 0) &&
    //            (cardInfo.startTime[1] == 0) &&
    //            (cardInfo.startTime[2] == 0)
    //            )
    //        {
    //            CommonFn.ShowToast(ToastrControl.toastrError, "บัตรยังไม่ได้ทำรายการขาเข้า");
    //            Debug.Log("New card found, No value to pay");
    //            return null;
    //        }

    //        response.gateId = cardInfo.gateId;
    //        DateTime enterDateTime = new DateTime();
    //        string[] parkingTime = FindParkingTotalTime(cardInfo, out enterDateTime);
    //        response.parkingHour = parkingTime[0];
    //        response.parkingMinute = parkingTime[1];
    //        response.enterDateTime = enterDateTime;
    //        response.exitDateTime = DateTime.Now;
    //        response.enterDate = enterDateTime.Day.ToString("D2") + "/" + enterDateTime.Month.ToString("D2") + "/" + enterDateTime.Year.ToString();
    //        response.enterTime = enterDateTime.Hour.ToString("D2") + ":" + enterDateTime.Minute.ToString("D2");
    //        response.exitDate = DateTime.Now.Day.ToString("D2") + "/" + DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year.ToString();
    //        response.exitTime = DateTime.Now.Hour.ToString("D2") + ":" + DateTime.Now.Minute.ToString("D2");
    //        string[] licensePrefixAndCode = GetLicencse(cardInfo.license);
    //        response.licensePrefix = licensePrefixAndCode[0];
    //        response.licenseCode = licensePrefixAndCode[1];
    //        response.cardId = (uint)((cardInfo.cardId[0] << 32)
    //                            | (cardInfo.cardId[1] << 16)
    //                            | (cardInfo.cardId[2] << 8)
    //                            | (cardInfo.cardId[3])
    //                            );
    //        response.cardTypeId = cardInfo.cardType;
    //        int[] stampIds = GetStampIds(cardInfo.stampCounter);
    //        string stampCode = "";
    //        CalFee.FeeInfo fee = null;
    //        //there is a case that stampcode deduct fee and discount is 0
    //        if (stampIds.Length > 0)
    //        {
    //            stampCode = FindBestStampCode(enterDateTime, cardInfo.cardType, stampIds);
    //            response.stampCode = stampCode;
    //            response.stampId = GetStampId(stampCode);
    //            fee = FindFeeFromStampDiscount(stampCode, enterDateTime, cardInfo.cardType);
    //        }
    //        else
    //        {
    //            fee = CalFee.CalculateParkingFee(enterDateTime, DateTime.Now, "", 0, cardInfo.cardType, 0, 0);
    //        }

    //        // check customer already paid or not
    //        response.fee = GetAmountPaid(cardInfo.paidCounter, fee);
    //        response.discount = fee.discount_baht;
    //        response.net = response.fee - response.discount;
    //        Debug.Log("Discount = " + response.discount.ToString());
    //        Debug.Log("Total fee = " + response.fee.ToString());
    //        Debug.Log("net = " + response.net.ToString());
    //        return response;
    //    }
    //    catch (Exception ex)
    //    {
    //        Debug.LogError(ex.ToString());
    //        throw ex;
    //    }
    //}

    public static byte[] ReadSector(int sector)
    {
        byte[] readData = new byte[0];
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            string[] dataReadBuffer = ajc.CallStatic<string[]>("ReadTagReturnStringArray", sector, Constant.passINNET);
            readData = Util.ToByteArray(dataReadBuffer[0] + dataReadBuffer[1] + dataReadBuffer[2] + dataReadBuffer[3]);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
        }

        return readData;
    }

    public static byte[] SubArrayDeepClone(byte[] data, int index, int length)
    {
        byte[] arrCopy = new byte[length];
        Array.Copy(data, index, arrCopy, 0, length);
        using (MemoryStream ms = new MemoryStream())
        {
            var bf = new BinaryFormatter();
            bf.Serialize(ms, arrCopy);
            ms.Position = 0;
            return (byte[])bf.Deserialize(ms);
        }
    }

    public static bool ClearCard(AndroidJavaClass ajc)
    {
        try
        {
            string data = "00000000000000000000000000000000";
            int clearBlock2 = ajc.CallStatic<int>("writeTagClick", 1, Constant.infoSector, data, Constant.passINNET);
            int clearBlock3 = ajc.CallStatic<int>("writeTagClick", 2, Constant.infoSector, data, Constant.passINNET);
            Debug.Log("clearBlock2 = " + clearBlock2);
            Debug.Log("clearBlock3 = " + clearBlock3);
            if ((clearBlock2 == 0) && (clearBlock3 == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static bool IsCardIdInSystem(byte[] cardId)
    {
        bool result = false;
        string cardIdStr = Util.ByteArrayToHexString(cardId);
        DataRow[] rows = Config.dtParkingCard.Select("UNIQUE_ID = '" + cardIdStr + "'");
        if (rows.Length > 0)
        {
            result = true;
        }
        else
        {
            Debug.LogError("card id " + cardIdStr + " is not in a system");
        }

        return result;
    }

    public static string GetUID()
    {
        var uid = "";
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            uid = ajc.CallStatic<string>("ReadUID");
            return uid;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }

        return uid;
    }

    public static string GetCardGroup(string cardTypeId)
    {
        var cardGroup = "";

        try
        {
            if (string.IsNullOrEmpty(cardTypeId)) return "";
            var rows = Config.dtCardtype.Select($"CARDTYPE_ID = {int.Parse(cardTypeId)}");
            if (rows is null || rows.Length == 0) return "";
            cardGroup = rows[0]["CARDGROUP_ID"].ToString();

        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }

        return cardGroup;
    }

    public static string GetCardType(string uid)
    {
        var cardType = "";

        try
        {
            var rows = Config.dtParkingCard.Select($"UNIQUE_ID = '{uid}'");
            if (rows is null || rows.Length == 0) return "";
            cardType = rows[0]["CARDTYPE_ID"].ToString();
            return cardType;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }

        return cardType;
    }

    public static string GetLicense(string uid)
    {
        try
        {
            var parkingCardRows = Config.dtParkingCard.Select($"UNIQUE_ID = '{uid}'");
            if (parkingCardRows.Length == 0) return "";
            var cardId = parkingCardRows[0]["PARKINGCARD_ID"].ToString();
            Debug.Log($"cardId = {cardId}");
            var parkingCardMapRows = Config.dtParkingCardMap.Select($"PARKINGCARD_ID = {cardId}");
            if (parkingCardMapRows.Length == 0) return "";
            var license = parkingCardMapRows[0]["VEHICLE_LICENSE"].ToString();
            return license;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static List<string> GetGatePass(string uid)
    {
        try
        {
            var gatePass = new List<string>();
            try
            {
                var rows = Config.dtParkingCard.Select($"UNIQUE_ID = '{uid}'");
                if (rows is null || rows.Length == 0) return new List<string>();
                gatePass = rows[0]["GATE_PASS"].ToString().Split(',').ToList();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
            }

            return gatePass;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static string GetCardNumber(string uid)
    {
        try
        {
            var cardNumber = "";
            var rows = Config.dtParkingCard.Select($"UNIQUE_ID = '{uid}'");
            if (rows is null || rows.Length == 0) return "";
            cardNumber = rows[0]["PARKINGCARD_CODE"].ToString();
            return cardNumber;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static bool IsCardEmpty(byte[] readData)
    {
        bool result = false;
        if ((readData[36] == 0) &&
            (readData[37] == 0) &&
            (readData[38] == 0) &&
            (readData[39] == 0) &&
            (readData[40] == 0) &&
            (readData[41] == 0)
        )
        {
            result = true;
        }

        return result;
    }

    public static bool IsCardActive(string uid)
    {
        try
        {
            var isActive = false;
            var rows = Config.dtParkingCard.Select($"UNIQUE_ID = '{uid}'");
            if (rows is null || rows.Length == 0) return false;
            var activeId = rows[0]["ACTIVE_ID"].ToString();
            if (activeId == "1") isActive = true;
            return isActive;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static bool IsMemberShareSite(CardData cardData)
    {
        try
        {
            if (cardData.enterDateTime == DateTime.MinValue &&
                cardData.exitDateTime == DateTime.MinValue &&
                !string.IsNullOrEmpty(cardData.cardNumber))
            {
                return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //obsolete
    private static string GetStampCode(int stampId, int cardTypeId, int subStampId)
    {
        //Debug.Log("stampId = " + stampId);
        //Debug.Log("cardTypeId = " + cardTypeId);
        //Debug.Log("subStampId = " + subStampId);
        string stampCode = "";
        DataRow[] row = Config.dtStampInfo.Select("STAMP_ID = '" + stampId + "' and SUB_STAMP_ID = '" + subStampId + "' and CARDTYPE_ID = '" + cardTypeId + "' and active_id = 'Y'");
        if (row.Length > 0)
        {
            stampCode = row[0]["STAMP_CODE"].ToString();
        }

        return stampCode;
    }

    public static ByteFormat CreateCardInfo(byte[] intputBytes)
    {
        try
        {
            ByteFormat cardInfo = new ByteFormat();
            cardInfo.cardType = intputBytes[4];
            //Debug.Log("card type = " + cardInfo.cardType.ToString());
            cardInfo.cardId = new byte[4];
            cardInfo.cardId[0] = intputBytes[5];
            cardInfo.cardId[1] = intputBytes[6];
            cardInfo.cardId[2] = intputBytes[7];
            cardInfo.cardId[3] = intputBytes[8];

            int index = 16;
            cardInfo.cardStatus = intputBytes[index++];
            cardInfo.stampCounter = intputBytes[index++];
            cardInfo.gateId = intputBytes[index++];
            cardInfo.provinceId = intputBytes[index++];
            Debug.Log("index license = " + index);
            cardInfo.license = new byte[16];
            for (int i = 0; i < cardInfo.license.Length; i++)
            {
                cardInfo.license[i] = intputBytes[index++];
            }

            cardInfo.startTime = new byte[6];
            for (int i = 0; i < cardInfo.startTime.Length; i++)
            {
                cardInfo.startTime[i] = intputBytes[index++];
            }

            cardInfo.paidCounter = intputBytes[index++];
            return cardInfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
