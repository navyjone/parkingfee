﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.ExitGate
{
    public class Shift
    {
        public static void SaveTransaction(Transaction.Exit inputExit)
        {
            var path = Constant.logShiftPath;
            var fileName =  Util.GetShiftId() + ".csv";
            var message = inputExit.arrive_date.ToString("dd-MM-yyyy HH:mm:ss") + "," +
                          inputExit.depart_date.ToString("dd-MM-yyyy HH:mm:ss") + "," + 
                          inputExit.license + "," + 
                          inputExit.parking_fee + "," + 
                          inputExit.stamp_discount + "," +
                          "\n";

            //Debug.Log("SaveTransaction file name = " + path + fileName);
            Util.WriteTextFile(path, fileName, message);
        }

        //public static void Archive(string shiftId)
        //{
        //    var source = Constant.logShiftActivePath + shiftId + ".csv";
        //    var destination = Constant.logShiftActivePath + shiftId + ".csv";
        //    if (File.Exists(source))
        //    {
        //        File.Move(source, destination);
        //    }
        //}

        public static PrintData.ShiftInput PreparePrintInput(DateTime logOutTime)
        {
            try
            {
                var fileName = Constant.logShiftPath + Util.GetShiftId() + ".csv";
                if(!File.Exists(fileName))
                {
                    Debug.LogWarning("no data in shift log");
                    // can' t return null
                    return new PrintData.ShiftInput
                    {
                        shiftId = ""
                    };
                }

                var contents = File.ReadAllText(fileName);
                var readTextList = contents.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None).ToList();

                var result = new PrintData.ShiftInput
                {
                    siteName = Util.GetHeaderSlip(),
                    cashierNumber = Util.GetCashChargeId(),
                    shiftId = Util.GetShiftId(),
                    LoginTime = Config.Get(Constant.lastLogin),
                    LogoutTime = logOutTime.ToString("yyyy-MM-dd HH:mm:ss")
                };

                var totalCar = readTextList.Count - 1;
                result.totalCar = totalCar.ToString();

                var totalAmount = 0;
                for (int i = 0; i < readTextList.Count; i++)
                {
                    if(readTextList[i] == "")
                    {
                        continue;
                    }

                    var inputSplit = readTextList[i].Split(',');
                    totalAmount+= int.Parse(inputSplit[3]) - int.Parse(inputSplit[4]);
                }

                result.totalAmount = totalAmount.ToString();
                return result;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
                throw ex;
            }
        }
    }
}
