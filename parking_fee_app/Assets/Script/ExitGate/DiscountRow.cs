﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class DiscountRow : MonoBehaviour
{
    public void OnClicked()
    {
        GameObject PaymentDetail = GameObject.Find("Canvas/ExitGatePanel/Body/PaymentDetail");
        string stampCode = this.gameObject.transform.GetChild(1).GetComponentInChildren<Text>().text;
        ((Payment)PaymentDetail.GetComponent(typeof(Payment))).SelectDiscountCallBack(stampCode);
    }
}

