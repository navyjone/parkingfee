﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class EStampManual : MonoBehaviour
{
    public struct Data
    {
        public string code;
        public string name;
    }

    private const int rowHeight = 120;

    public static string IsDisplay()
    {
        try
        {
            string result = "";
            DataRow[] rows = Config.dtSetting.Select("SETTING_NAME = 'IsStampManual'");
            result = rows[0]["SETTING_VALUE"].ToString();
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void CreateDiscountRow(GameObject DiscountScrollViewContent)
    {
        try
        {
            int heightScroll = 0;
            DataRow[] rows = Config.dtStampInfo.Select("CARDTYPE_ID = '1' and STAMP_ID <> '1'");
            for (int i = 0; i < rows.Length; i++)
            {
                Data inputData = new Data();
                inputData.code = rows[i]["STAMP_CODE"].ToString();
                inputData.name = rows[i]["STAMP_NAME"].ToString();
                AddDiscountRow(i, inputData, DiscountScrollViewContent);
                heightScroll = (i * rowHeight) + rowHeight;
            }

            RectTransform rt = DiscountScrollViewContent.GetComponent<RectTransform>();
            Vector2 currentPos = rt.sizeDelta;
            currentPos.y = heightScroll;
            rt.sizeDelta = currentPos;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private static void AddDiscountRow(int index, Data inputData, GameObject DiscountScrollViewContent)
    {
        GameObject instance = Instantiate(Resources.Load("Prefabs/DiscountRow", typeof(GameObject))) as GameObject;
        instance.transform.SetParent(DiscountScrollViewContent.transform);
        instance.transform.GetChild(0).GetComponentInChildren<Text>().text = inputData.code + " " + inputData.name;
        instance.transform.GetChild(1).GetComponentInChildren<Text>().text = inputData.code;
        instance.name = "DiscountRow" + index.ToString();
        //int height = (int)instance.GetComponent<RectTransform>().rect.height;
        instance.GetComponent<RectTransform>().offsetMax = new Vector2(0f, 0f);
        instance.GetComponent<RectTransform>().offsetMin = new Vector2(0f, -rowHeight);
        instance.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.0f, (float)(index * -rowHeight) - 64, 0.0f);
        instance.GetComponent<RectTransform>().localScale = Vector3.one;
        Resources.UnloadUnusedAssets();
    }
}

