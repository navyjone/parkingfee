﻿using Assets.Script.ExitGate;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Payment : MonoBehaviour {
    public Text LicensePrefixTx;
    public Text LicenseCodeTx;
    public Text EnterDateTx;
    public Text EnterTimeTx;
    public Text ExitDateTx;
    public Text ExitTimeTx;
    public Text ParkingHourTx;
    public Text ParkingMinuteTx;
    public Text FeeTx;
    public Text StampCodeTx;
    public Text DiscountTx;
    public Text NetTx;
    public GameObject PayBt;
    public GameObject PaymentPanel;
    public GameObject Summary;
    public GameObject MainEvent;
    public GameObject DiscountPanel;
    public Text CardNumberTx;

    private CardData card;

    public void Initial(CardData card) {
        try
        {
            LicensePrefixTx.text = card.licensePrefix;
            LicenseCodeTx.text = card.licenseCode;
           
            if(card.enterDateTime != DateTime.MinValue)
            {
                EnterDateTx.text = card.enterDateTime.ToString("dd/MM");
                EnterTimeTx.text = card.enterDateTime.ToString("hh:mm");
            }
            else
            {
                EnterDateTx.text = "";
                EnterTimeTx.text = "";
            }

            if(card.exitDateTime != DateTime.MinValue)
            {
                ExitDateTx.text = card.exitDateTime.ToString("dd/MM");
                ExitTimeTx.text = card.exitDateTime.ToString("hh:mm");
            }
            else
            {
                ExitDateTx.text = "";
                ExitTimeTx.text = "";
            }

            ParkingHourTx.text = card.parkingHour;
            ParkingMinuteTx.text = card.parkingMinute;
            FeeTx.text = card.fee.ToString("N0");
            DiscountTx.text = card.discount.ToString("N0");
            NetTx.text = card.net.ToString("N0");
            PayBt.SetActive(true);
            StampCodeTx.text = card.stampCode;
            CardNumberTx.text = card.cardNumber;
            this.card = card;
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการแสดงหน้าจ่ายเงิน");
        }
    }

    public void PayBtClick() 
    {
        this.ClearPaymentInput();     
        PaymentPanel.SetActive(true);
    }

    public void NextBtClick(Text paidTx) 
    {
        // Calculate summary
        try
        {
            if (paidTx.text == "")
            {
                this.card.paid = 0;
            }
            else
            {
                this.card.paid = int.Parse(paidTx.text);
            }

            this.card.change = this.card.paid - (this.card.fee - this.card.discount);

            if (this.card.fee == 0)
            {
                AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
                if (CardData.IsMemberShareSite(this.card) || CardData.ClearCard(ajc))
                {
                    Transaction.Exit[] exitData = Transaction.PrepareExit(this.card, "", 0);
                    Transaction.Send(exitData, Constant.ExitGate);
                    Shift.SaveTransaction(exitData[0]);
                    CommonFn.ShowToast(ToastrControl.toastrSuccess, "ทำรายการเสร็จสิ้น");
                    ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).Initial();
                }
                else
                {
                    CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถล้างข้อมูลบัตรได้ โปรดทำรายการใหม่อีกครั้ง");
                }

                ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).Initial();
                return;
            }

            //Debug.Log("change = " + this.card.change);
            if (this.card.change >= 0)
            {
                // Close payment
                PaymentPanel.SetActive(false);
                this.gameObject.SetActive(false);

                // Open summary
                Summary.SetActive(true);
                ((Summary)Summary.GetComponent(typeof(Summary))).Initial(this.card);
                this.card = null;
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "โปรดตรวจสอบการจ่ายเงิน");
            }
        }
        catch (FormatException ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการคิดเงินทอน");
        }
    }

    public void CloseBtClick(GameObject obj)
    {
        obj.SetActive(false);       
    }

    public void DiscountBtClick()
    {
        DiscountPanel.SetActive(true);
    }

    public void SelectDiscountCallBack(string stampCode)
    {
        try
        {
            Debug.Log("SelectDiscountCallBack");
            Debug.Log("before discount fee = " + this.card.fee);
            //Debug.Log("enterDateTime = " + this.card.enterDateTime);
            //Debug.Log("exitDateTime = " + this.card.exitDateTime);
            Debug.Log("stampCode = " + stampCode);
            Debug.Log("cardTypeId = " + this.card.cardTypeId);
            if (this.card.fee > 0)
            {
                CalFee.FeeInfo newFee = CalFee.CalculateParkingFee(this.card.enterDateTime, this.card.exitDateTime, stampCode, 0, this.card.cardTypeId, 0, 0);
                Debug.Log("discount = " + newFee.discount_baht);
                this.card.discount = newFee.discount_baht;
                if(this.card.fee > newFee.ParkingFee)
                {
                    this.card.discount = (this.card.fee - newFee.ParkingFee) + newFee.discount_baht;
                    Debug.Log("new parking fee = " + newFee.ParkingFee);
                    Debug.Log(" discount that cause new parkingFee change = " + this.card.discount);
                }

                this.card.net = this.card.fee - this.card.discount;
                this.card.stampCode = stampCode;
                DiscountTx.text = this.card.discount.ToString();
                NetTx.text = this.card.net.ToString();
                StampCodeTx.text = stampCode;
            }
         
            DiscountPanel.SetActive(false);
        }
        catch (FormatException ex)
        {
            Debug.LogError(ex.ToString());
            // TODO: do something
        }
    }

    private void ClearPaymentInput()
    {
        var input = PaymentPanel.transform.Find("Payment/Input").GetComponent<InputField>();
        input.Select();
        input.text = "";
    }

    private string CutYear(string input)
    {
        try
        {
            string[] dateSplits = input.Split('/');
            string result = dateSplits[0] + "/" + dateSplits[1];
            return result;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
}