﻿using Assets.Script.ExitGate;
using System;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class Summary : MonoBehaviour {
    public GameObject StatusIcon;
    public GameObject PrinterIcon;
    public Text NetTx;
    public Text PaidTx;
    public Text ChangeTx;
    public Text UserTx;
    public GameObject SubmitBt;
    public GameObject CancelBt;
    public GameObject ReprintBt;
    public GameObject BackBt;
    public GameObject Payment;
    public GameObject MainEvent;
    public GameObject SuccessPanel;
    
    private CardData card;
    private PrintData.ExitGateInput printInput;
    private string receiptNumber;
    private AndroidJavaObject activityContext = null;

    public void Initial(CardData card)
    {
        //user name has cover back button
        UserTx.text = "";
        StatusIcon.SetActive(false);
        PrinterIcon.SetActive(false);
        NetTx.text = card.net.ToString("N0");
        PaidTx.text = card.paid.ToString("N0");
        ChangeTx.text = card.change.ToString("N0");
        SubmitBt.SetActive(true);
        CancelBt.SetActive(true);
        ReprintBt.SetActive(false);
        BackBt.SetActive(false);
        this.card = card;
    }

    public void SubmitBtClick()
    {
        try
        {
            //SuccessPanel.SetActive(true);
            SubmitBt.SetActive(false);
            CancelBt.SetActive(false);
            ReprintBt.SetActive(true);
            BackBt.SetActive(true);
            CommonFn.ShowLoading(true);
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            if(CardData.IsMemberShareSite(this.card) || CardData.ClearCard(ajc))
            {
                this.receiptNumber = GetReceiptNumber();
                Debug.Log("receiptNumber=" + receiptNumber);
                Transaction.Exit[] exitData = Transaction.PrepareExit(this.card, this.receiptNumber, this.card.paid);
                Transaction.Send(exitData, Constant.ExitGate);
                Shift.SaveTransaction(exitData[0]);
                PreparePrintInput(this.card, this.receiptNumber);
                PrinterZKC.PrintExit(this.printInput);
                CommonFn.ShowLoading(false);
                //PrinterBluetooth.Print(this.printInput, Config.Get(Constant.activePrinter), Constant.ExitGate, activityContext);
            }
            else
            {
                CommonFn.ShowLoading(false);
                CommonFn.ShowToast(ToastrControl.toastrError, "ไม่สามารถล้างข้อมูลบัตรได้ โปรดทำรายการใหม่อีกครั้ง");
                ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).Initial();
            }
        }
        catch(Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการพิมพ์หรือการส่งข้อมูลเข้าส่วนกลาง");
            Debug.LogError(ex.ToString());
        }
    }

    public void CancelBtClick()
    {
        this.gameObject.SetActive(false);
        Payment.SetActive(true);
        ((Payment)Payment.GetComponent(typeof(Payment))).Initial(this.card);
        this.card = null;
    }

    public void BackBtClick()
    {
        ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).Initial();
    }

    public void ReprintBtClick()
    {
        try
        {
            CommonFn.ShowLoading(true);
            //AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjReader);
            //CardData.ClearCard(ajc);
            PrinterZKC.PrintExit(this.printInput);
            CommonFn.ShowLoading(false);
            //PrinterBluetooth.Print(this.printInput, Config.Get(Constant.activePrinter), Constant.ExitGate, activityContext);
        }
        catch(Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการสั่งพิมพ์อีกครั้ง");
            Debug.LogError(ex.ToString());
        }
    }

    private void PreparePrintInput(CardData cardData, string repNumber)
    {
        try
        {
            if(card.enterDateTime != DateTime.MinValue)
            {
                this.printInput.enterDate = cardData.enterDateTime.ToString("dd/MM");
            }

            if (card.exitDateTime != DateTime.MinValue)
            {
                this.printInput.exitDate = cardData.exitDateTime.ToString("dd/MM");
            }

            this.printInput.license = cardData.licensePrefix + cardData.licenseCode;
            this.printInput.parkingFee = cardData.fee;
            this.printInput.stampCode = cardData.stampCode;
            this.printInput.stampDiscount = cardData.discount;
            this.printInput.siteName = Util.GetHeaderSlip();
            //print("site name = " + this.printInput.siteName);
            this.printInput.address = Config.dtSysteminfo.Rows[0][Constant.address].ToString();
            //print("address = " + this.printInput.address);
            this.printInput.posId = Util.GetCashChargeId();
            this.printInput.receiptId = this.receiptNumber;
            this.printInput.gateName = this.GetExitGateName();
            //print("gateName = " + this.printInput.gateName);
            this.printInput.taxId = Config.dtSysteminfo.Rows[0][Constant.taxId].ToString();
            this.printInput.cashReceive = cardData.paid;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private string GetReceiptNumber()
    {
        try
        {
            //MMYY{GATE ID}00001
            string result = "";
            Dictionary<string, string> inputSave = new Dictionary<string, string>();
            string prefixFromConfig = Config.Get(Constant.repceiptPrefix);
            DateTime prefixDateTime = DateTime.Parse(prefixFromConfig);
            int year = DateTime.Now.Year - 2000;
            string prefixSlip = DateTime.Now.Month.ToString("D2") + year.ToString("D2");
            int gateId = int.Parse(Config.Get(Constant.exitGateId));

            if ((prefixDateTime.Year == DateTime.Now.Year && prefixDateTime.Month < DateTime.Now.Month)
               || prefixDateTime.Year < DateTime.Now.Year)
            {
                //reset running number
                result = prefixSlip + gateId.ToString("D2") + "00001";
                inputSave.Add(Constant.receiptNumber, "1");
                inputSave.Add(Constant.repceiptPrefix, DateTime.Now.ToString("yyyy-MM-dd"));
                Config.Save(inputSave);
            }
            else
            {
                int number = int.Parse(Config.Get(Constant.receiptNumber));
                number++;
                result = prefixSlip + gateId.ToString("D2") + number.ToString("D5");
                inputSave.Add(Constant.receiptNumber, number.ToString());
                Config.Save(inputSave);
            }

            return result;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private string GetExitGateName()
    {
        try
        {
            string result = "";
            string gateId = Config.Get(Constant.exitGateId);
            DataRow[] rows = Config.dtGateName.Select("ID = '"+ gateId + "'");
            result = rows[0]["NAME"].ToString();
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}