﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using UnityEngine.UI;
using System.Data;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;
using System.Net.NetworkInformation;

public class MainEventLogin : MonoBehaviour
{
    public InputField userInput;
    public InputField passwordInput;
    public Text lastSyncTx;
    public delegate string AsyncMethodCaller();
    System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
    private string lastSync = "";
    private bool displayLastSync = false;

    // Use this for initialization
    void Start()
    {
        try
        {
            Debug.Log("xml path = " + Constant.XMLPath);
            Config.LoadXmlToDataTable();
            Config.InitialDevice();
            lastSyncTx.text = "เชื่อมต่อครั้งล่าสุด " + Config.Get(Constant.lastSync);
            string activeUserId = Config.Get(Constant.activeUserId);
            if(!string.IsNullOrEmpty(activeUserId))
            {
                string activeGateId = Config.Get(Constant.activeGateId);
                ShareInstance.userName = this.GetNameUserById(activeUserId);
                Debug.Log("ShareInstance.userName =" + ShareInstance.userName);
                if (!string.IsNullOrEmpty(activeGateId))
                {
                    CommonFn.ShowLoading(true);
                    string enterGateId = Config.Get(Constant.enterGateId);
                    string exitGateId = Config.Get(Constant.exitGateId);
                    if ((activeGateId == enterGateId) && (!string.IsNullOrEmpty(enterGateId)))
                    {
                        SceneManager.LoadScene(Constant.EnterGate);
                    }
                    else if ((activeGateId == exitGateId) && (!string.IsNullOrEmpty(exitGateId)))
                    {
                        SceneManager.LoadScene(Constant.ExitGate);
                    }
                    else
                    {
                        SceneManager.LoadScene(Constant.SelectMode);
                    }
                }
                else
                {
                    SceneManager.LoadScene(Constant.SelectMode);
                }
            }
        }
        catch(Exception ex)
        {
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการดึงข้อมูลจากไฟล์ XML");
            Debug.LogError(ex.ToString());
        }
    }

    void Update()
    {
        if(this.displayLastSync)
        {
            this.displayLastSync = false;
            lastSyncTx.text = this.lastSync;
        }

        if (stopwatch.Elapsed.TotalSeconds >= 5)
        {
            stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            Transaction.ResendFailTransaction();
        }

        if (Application.platform == RuntimePlatform.Android)
        {

            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {

                // Quit the application
                Application.Quit();
            }
        }
    }

    public void LoginBtClicked()
    {
        try
        {
            DataRow[] row = Config.dtUser.Select("username = '" + userInput.text + "' and password = '" + passwordInput.text + "'");
            if (row.Length > 0)
            {
                ShareInstance.userId = int.Parse(row[0]["USER_ID"].ToString());
                ShareInstance.userName = row[0]["NAME"].ToString();
                Debug.Log("ShareInstance.userName =" + ShareInstance.userName);
                print("login by userId =" + ShareInstance.userId);
                Dictionary<string, string> inputSave = new Dictionary<string, string>();
                inputSave.Add(Constant.activeUserId, ShareInstance.userId.ToString());
                Config.Save(inputSave);
                SceneManager.LoadScene(Constant.SelectMode);
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "ไม่มีผู้ใช้งานหรือรหัสผ่านอยู่ในระบบ");
            }
        }
        catch(Exception ex)
        {
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการเข้าใช้งาน");
            Debug.LogError(ex.ToString());
        }
    }

    public void RefreshBtClicked()
    {
        try
        {
            CommonFn.ShowLoading(true);
            AsyncMethodCaller caller = new AsyncMethodCaller(RequestAll);
            AsyncCallback completedCallback = new AsyncCallback((ar) =>
            {
                var result = caller.EndInvoke(ar);
                Debug.Log("result request xml = " + result);
                if (result == TcpConnection.Status.success)
                {
                    CommonFn.ShowToast(ToastrControl.toastrSuccess, "เชื่อมต่อข้อมูลกับส่วนกลางสำเร็จ");
                    string dateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Dictionary<string, string> inputSave = new Dictionary<string, string>
                    {
                        { Constant.lastSync, dateTime }
                    };
                    Config.Save(inputSave);
                    this.lastSync = "เชื่อมต่อครั้งล่าสุด " + dateTime;
                    this.displayLastSync = true;
                }
                else
                {
                    CommonFn.ShowToast(ToastrControl.toastrError, "เชื่อมต่อข้อมูลกับส่วนกลางล้มเหลว");
                    //Debug.LogError(result);
                }

                CommonFn.ShowLoading(false);
            });

            caller.BeginInvoke(new AsyncCallback(completedCallback), null);
        }
        catch (Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดเชื่อมต่อข้อมูลกับส่วนกลาง");
            Debug.LogError(ex.ToString());
        } 
    }

    private string RequestAll()
    {
        //async caller need string
        var result = Config.RequestAll();
        return result;
    }

    private string GetNameUserById(string activeUserId)
    {
        try
        {
            string result = "";
            DataRow[] row = Config.dtUser.Select("USER_ID = '" + activeUserId + "'");
            if (row.Length > 0)
            {
                result = row[0]["NAME"].ToString();
            }

            return result;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
}
