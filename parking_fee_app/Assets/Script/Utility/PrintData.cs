﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class PrintData
{
    public struct ExitGateInput
    {
        public string siteName;
        public string taxId;
        public string posId;
        public string receiptId;
        public string license;
        public string enterDate;
        public string exitDate;
        public int parkingFee;
        public int penaltyFee;
        public string stampCode;
        public int stampDiscount;
        public int couponDiscount;
        public int cashReceive;
        public string gateName;
        public string address;
        public string branch;
    }

    public struct ShiftInput
    {
        public string siteName;
        public string totalCar;
        public string cashierNumber;
        public string LoginTime;
        public string LogoutTime;
        public string shiftId;
        public string totalAmount;
    }
}

