﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class CommonFn : MonoBehaviour {

    public GameObject loading;

    private class Loading
    {
        public static bool display = false;
        public static bool isShown = false;
    }

    private class Toast
    {
        public static bool display = false;
        public static string message = "";
        public static Color32 type = ToastrControl.toastrSuccess;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Loading.display)
        {
            Loading.display = false;
            loading.SetActive(Loading.isShown);
        }

        if (Toast.display)
        {
            Toast.display = false;
            ToastrControl.toastrBackground = Toast.type;
            ToastrControl.toastrMessage = Toast.message;
            ToastrControl.toastrShow = true;
        }
    }

    public static void ShowLoading(bool isShown)
    {
        Loading.display = true;
        Loading.isShown = isShown;
    }

    public static void ShowToast(Color32 type, string message)
    {
        Toast.display = true;
        Toast.message = message;
        Toast.type = type;
    }

    public void SceneLoad (string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public static string ReadTextFromDevice(string path)
    {
        string str = "";
        try
        {
            using (StreamReader sr = File.OpenText(path))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    if (s != "")
                    {
                        str += s;
                    }
                }
            }
            return str;
        }
        catch (Exception e)
        {
            Debug.Log(e);

        }
        return "";
    }
}
