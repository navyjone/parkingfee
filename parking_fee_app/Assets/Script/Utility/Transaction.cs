﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using RestSharp;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System.Data;
using System;
using System.IO;
using Newtonsoft.Json;

public class Transaction
{
    public struct Exit
    {
        public uint unique_id;
        public DateTime arrive_date;
        public DateTime depart_date;
        public int stamp_id;
        public int coupon_subtract;
        public int sub_stamp_id;
        public int point_id;
        public int stamp_discount;
        public int parking_fee;
        public int penalty;
        public int costCardLost;
        public int costCardDamaged;
        public int total_discount;
        public int non_cash_pay;
        public int cash_pay;
        public int user_id;
        public double vatrate;
        public string tax_invoice;
        public int tax_invoice_fee;
        public string tax_invoice2;
        public int tax_invoice_fee2;
        public string cashcharge_id;
        public uint EStampCardNumber;
        public string license;
        public int enter_gate;
        public int exit_gate;
        public string shift_id;
        public int province_id;
        public int tax_invoice_length;
        public int tax_invoice_length2;
        public bool cancel_fee;
    }

    public struct Enter
    {
        public DateTime enterDatetime;
        public int gateId;
        public string license;
        public uint cardNumber;
        public int userId;
        public string shiftId;
    }

    public struct Login
    {
        public DateTime loginDatetime;
        public DateTime logoutDatetime;
        public int gateId;
        public int userId;
    }

    //private static string serverUrl = "http://localhost:50870/api/transaction";
    //private static string serverUrl = "http://navyjone.com/WebAPIParkingFeeTest/api/transaction";

    public static void Send(object inputs, string scene, bool isResend = false)
    {
        try
        {
            //string serverUrl = "http://" + Config.GetDevice(Constant.apiUrl) + "/WebAPIParkingFeeTest/api/transaction";
            string serverUrl = Config.Get(Constant.apiUrl);
            //Debug.Log("serverUrl = " + serverUrl);
            string controller = "";
            switch (scene)
            {
                case Constant.ExitGate :
                    controller = "MobileCarExitApi";
                    break;
                case Constant.EnterGate :
                    controller = "MobileCarEntranceApi";
                    break;
                case Constant.Login :
                    controller = "MobileShifteApi";
                    break;
            }

            //Debug.Log("Send Transaction API = " + apiAction);
            var client = new RestClient(serverUrl + controller);
            RestRequest request = new RestRequest("POST", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(inputs), ParameterType.RequestBody);
            client.Timeout = 500;
            client.ExecuteAsync(request, response => {
                //Debug.Log("response.StatusCode = " + response.StatusCode);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //Debug.Log("response ok");
                    //Log success if resend fail transaction move all log fail to log success folder
                    if (isResend)
                    {
                        Debug.Log("Delete Folder After Resend Success");
                        DeleteFolder(scene);
                    }

                    Log(inputs, scene, isLogSuccess: true);
                }
                else
                {
                    //Debug.LogWarning("send transaction fail = " + response.StatusCode);
                    //Debug.LogWarning("isResend = " + isResend);
                    if (!isResend)
                    {
                        Log(inputs, scene, isLogSuccess: false);
                    }
                }
            });
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public static Exit[] PrepareExit(CardData cardData, string repNumber, int cash)
    {
        try
        {
            Transaction.Exit[] transactions = new Transaction.Exit[1];
            transactions[0] = new Transaction.Exit();
            transactions[0].unique_id = cardData.cardId;
            transactions[0].arrive_date = cardData.enterDateTime;
            transactions[0].depart_date = cardData.exitDateTime;
            transactions[0].stamp_id = cardData.stampId;
            transactions[0].coupon_subtract = 0;
            transactions[0].sub_stamp_id = 0;
            transactions[0].point_id = 0;
            transactions[0].stamp_discount = cardData.discount;
            transactions[0].parking_fee = cardData.fee;
            transactions[0].penalty = 0;
            transactions[0].costCardLost = 0;
            transactions[0].costCardDamaged = 0;
            transactions[0].total_discount = cardData.discount;
            transactions[0].non_cash_pay = 0;
            transactions[0].cash_pay = cash;
            transactions[0].user_id = int.Parse(Config.Get(Constant.activeUserId));
            string vat = Config.dtSysteminfo.Rows[0][Constant.vat].ToString();
            //print("vat = " +vat);
            transactions[0].vatrate = double.Parse(vat);
            transactions[0].tax_invoice = repNumber;
            //TODO calculate vat price later
            transactions[0].tax_invoice_fee = cash;
            transactions[0].tax_invoice2 = "";
            transactions[0].tax_invoice_fee2 = 0;
            transactions[0].cashcharge_id = Util.GetCashChargeId();
            transactions[0].EStampCardNumber = 0;
            transactions[0].license = cardData.licensePrefix + cardData.licenseCode;
            transactions[0].enter_gate = cardData.gateId;
            transactions[0].exit_gate = int.Parse(Config.Get(Constant.exitGateId));
            transactions[0].shift_id = Util.GetShiftId();
            transactions[0].province_id = 0;
            transactions[0].tax_invoice_length = repNumber.Length;
            transactions[0].tax_invoice_length2 = 0;
            transactions[0].cancel_fee = false;
            return transactions;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void ResendFailTransaction()
    {
        try
        { 
            List<string> exitTransactions = ReadFolder(Constant.ExitGate);
            //Debug.Log("exitTransactions.lenght = " + exitTransactions.Count);
            if(exitTransactions.Count > 0)
            {
                Exit[] data = PrepareExitFromLog(exitTransactions);
                Transaction.Send(data, Constant.ExitGate, true);
            }

            List<string> enterTransactions = ReadFolder(Constant.EnterGate);
            //Debug.Log("enterTransactions.lenght = " + enterTransactions.Count);
            if (enterTransactions.Count > 0)
            {
                Enter[] data = PrepareEnterFromLog(enterTransactions);
                Transaction.Send(data, Constant.EnterGate, true);
            }

            List<string> LoginTransactions = ReadFolder(Constant.Login);
            //Debug.Log("LoginTransactions.lenght = " + LoginTransactions.Count);
            if (LoginTransactions.Count > 0)
            {
                Login[] data = PrepareLoginFromLog(LoginTransactions);
                Transaction.Send(data, Constant.Login, true);
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public static List<string> ReadFolder(string scene)
    {
        string path = "";
        switch (scene)
        {
            case "ExitGate":
                path = Constant.logFailExitPath;
                break;
            case "EnterGate":
                path = Constant.logFailEnterPath;
                break;
            case "Login":
                path = Constant.logFailLoginPath;
                break;
        }

        List<string> results = new List<string>();
        if (Directory.Exists(path))
        {
            string[] files = Directory.GetFiles(path, "*.csv", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                string contents = File.ReadAllText(file);
                string[] lines = contents.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                for (int i = 0; i < lines.Length; i++)
                {
                    results.Add(lines[i]);
                }
            }
        }

        return results;
    }

    private static Exit[] PrepareExitFromLog(List<string> inputs)
    {
        try
        {
            int time = inputs.Count - 1;
            Exit[] info = new Exit[time];

            for (int i = 0; i < time; i++)
            {
                //Debug.Log("prepare time = " + i);
                if (inputs[i] == "")
                {
                    continue;
                }

                string[] inputSplit = inputs[i].Split(',');
                int indexInput = 0;
                info[i].unique_id = uint.Parse(inputSplit[indexInput++]);
                //Debug.Log("arrive_date = " + inputSplit[1]);
                info[i].arrive_date = DateTime.Parse(inputSplit[indexInput++]);
                //Debug.Log("depart_date = " + inputSplit[2]);
                info[i].depart_date = DateTime.Parse(inputSplit[indexInput++]);
                info[i].stamp_id = int.Parse(inputSplit[indexInput++]);
                info[i].coupon_subtract = 0;
                info[i].sub_stamp_id = 0;
                info[i].point_id = 0;
                info[i].stamp_discount = int.Parse(inputSplit[indexInput++]);
                info[i].parking_fee = int.Parse(inputSplit[indexInput++]);
                info[i].penalty = 0;
                info[i].costCardLost = 0;
                info[i].costCardDamaged = 0;
                info[i].total_discount = int.Parse(inputSplit[indexInput++]);
                info[i].non_cash_pay = 0;
                info[i].cash_pay = int.Parse(inputSplit[indexInput++]);
                info[i].user_id = int.Parse(inputSplit[indexInput++]);
                info[i].vatrate = double.Parse(inputSplit[indexInput++]);
                info[i].tax_invoice = null;
                //TODO calculate vat price later
                info[i].tax_invoice_fee = 0;
                info[i].tax_invoice2 = "";
                info[i].tax_invoice_fee2 = 0;
                info[i].cashcharge_id = inputSplit[indexInput++];
                info[i].EStampCardNumber = 0;
                info[i].license = inputSplit[indexInput++];
                info[i].exit_gate = int.Parse(inputSplit[indexInput++]);
                info[i].shift_id = inputSplit[indexInput++];
                info[i].province_id = 0;
                info[i].tax_invoice_length = 0;
                info[i].tax_invoice_length2 = 0;
                info[i].cancel_fee = false;
            }

            return info;
        }
        catch(Exception ex)
        {
            Debug.LogError(ex);
            throw ex;
        }
    }

    private static Enter[] PrepareEnterFromLog(List<string> inputs)
    {
        try
        {
            int time = inputs.Count - 1;
            Enter[] info = new Enter[time];
            for (int i = 0; i < time; i++)
            {
                //Debug.Log("prepare time = " + i);
                if (inputs[i] == "")
                {
                    continue;
                }

                string[] inputSplit = inputs[i].Split(',');
                int indexInput = 0;
                info[i].enterDatetime = DateTime.Parse(inputSplit[indexInput++]);
                info[i].gateId = int.Parse(inputSplit[indexInput++]);
                info[i].license = inputSplit[indexInput++];
                info[i].cardNumber = uint.Parse(inputSplit[indexInput++]);
                info[i].userId = int.Parse(inputSplit[indexInput++]);
                info[i].shiftId = inputSplit[indexInput++];
            }

            return info;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            throw ex;
        }
    }

    private static Login[] PrepareLoginFromLog(List<string> inputs)
    {
        try
        {
            int time = inputs.Count - 1;
            Login[] info = new Login[time];
            for (int i = 0; i < time; i++)
            {
                if(inputs[i] == "")
                {
                    continue;
                }

                string[] inputSplit = inputs[i].Split(',');
                int indexInput = 0;
                info[i].loginDatetime = DateTime.Parse(inputSplit[indexInput++]);
                info[i].logoutDatetime = DateTime.Parse(inputSplit[indexInput++]);
                info[i].gateId = int.Parse(inputSplit[indexInput++]);
                info[i].userId = int.Parse(inputSplit[indexInput++]);
            }

            return info;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            throw ex;
        }
    }

    private static void Log(object inputInfo, string scene, bool isLogSuccess)
    {
        try
        {
            string fileName = "";
            string message = "";
            string path = "";

            if (scene == Constant.ExitGate)
            {
                //Debug.Log("prepare log ExitGate");
                if (isLogSuccess)
                {
                    fileName = "exit_success_";
                    path = Constant.logSuccessExitPath;
                }
                else
                {
                    //Debug.Log("prepare log fail");
                    fileName = "exit_fail_";
                    path = Constant.logFailExitPath;
                }

                fileName += DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
                Exit[] exits = (Exit[])inputInfo;
                for (int i = 0; i < exits.Length; i++)
                {
                    message += exits[i].unique_id + "," + //cardId
                              exits[i].arrive_date.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                              exits[i].depart_date.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                              exits[i].stamp_id + "," +
                              exits[i].stamp_discount + "," +
                              exits[i].parking_fee + "," +
                              exits[i].total_discount + "," +
                              exits[i].cash_pay + "," +
                              exits[i].user_id + "," +
                              exits[i].vatrate + "," +
                              exits[i].cashcharge_id + "," +
                              exits[i].license + "," +
                              exits[i].exit_gate + "," +
                              exits[i].shift_id +
                              "\n";
                }
            }
            else if (scene == Constant.EnterGate)
            {
                //Debug.Log("prepare log enter");
                if (isLogSuccess)
                {
                    fileName = "enter_success_";
                    path = Constant.logSuccessEnterPath;
                }
                else
                {
                    //Debug.Log("prepare log fail");
                    fileName = "enter_fail_";
                    path = Constant.logFailEnterPath;
                }

                fileName += DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
                Enter[] enters = (Enter[])inputInfo;
                for (int i = 0; i < enters.Length; i++)
                {
                    message += enters[i].enterDatetime.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                              enters[i].gateId + "," +
                              enters[i].license + "," +
                              enters[i].cardNumber + "," +
                              enters[i].userId + "," +
                              enters[i].shiftId +
                              "\n";
                }
            }
            else if (scene == Constant.Login)
            {
                //Debug.Log("prepare log login");
                if (isLogSuccess)
                {
                    fileName = "login_success_";
                    path = Constant.logSuccessLoginPath;
                }
                else
                {
                    //Debug.Log("prepare log fail");
                    fileName = "login_fail_";
                    path = Constant.logFailLoginPath;
                }

                fileName += DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
                Login[] logins = (Login[])inputInfo;
                for (int i = 0; i < logins.Length; i++)
                {
                    message += logins[i].loginDatetime.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                              logins[i].logoutDatetime.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                              logins[i].gateId + "," +
                              logins[i].userId +
                              "\n";
                }
            }

            //Debug.Log("log success path = " + logSuccessPath + " message = " + message);
            Util.WriteTextFile(path, fileName, message);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private static void DeleteFolder(string scene)
    {
        string path = "";
        switch (scene)
        {
            case "ExitGate":
                path = Constant.logFailExitPath;
                break;
            case "EnterGate":
                path = Constant.logFailEnterPath;
                break;
            case "Login":
                path = Constant.logFailLoginPath;
                break;
        }

        DirectoryInfo directoryInfo = new DirectoryInfo(path);
        foreach (FileInfo file in directoryInfo.GetFiles())
        {
            file.Delete();
        }

        foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
        {
            dir.Delete(true);
        }
    }
}

