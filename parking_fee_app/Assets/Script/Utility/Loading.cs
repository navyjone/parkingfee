﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Loading : MonoBehaviour {

    public Image loadingImage;
    public float timeUp = 100.0f;

    private RectTransform loadingRec;
    private float watchDog = 0.0f;
    private float rotateTime = 5.0f;

	// Use this for initialization
	void Start () {
        loadingRec = loadingImage.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (watchDog >= timeUp)
        {
            float NewZAxis = (loadingRec.rotation.z - 0.05f);
            Quaternion target = new Quaternion(0.0f, 0.0f, NewZAxis, 0.0f);
            loadingRec.rotation = Quaternion.AngleAxis(-45 * rotateTime, Vector3.forward);
            
            //reset watchdog
            watchDog = 0.0f;
            rotateTime++;
            if (rotateTime > 80)
            {
                Debug.Log("reset Rotate");
                rotateTime = 1.0f;
            }
            return;
        }
        //add watchdog
        watchDog += 1.0f;      
    }
}
