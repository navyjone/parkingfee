﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UnityEngine;

public class CalFee
{
    public class FeeInfo
    {
        public int Parking_Minute;
        public int ParkingFee = 0;
        public int discount_baht = 0;
        public int discount_minute = 0;
        public int StampDiscount = 0;
        public int CouponDiscount = 0;
        public int PointDiscount = 0;
        public int CardLossFee = 0;
        public int CardDamagedFee = 0;
        public int FeeScheduleID = 0;
        public int Overnight = 0;
        public int TotalFee = 0;
        public string StampCode = "00000";
        public int StampID = 0;
        public int SubStampID = 0;
        public int FeeWithoutDiscount = 0;
        public UInt32 PayStationPaid = 0;

        public FeeInfo()
        {
        }
    }

    public static FeeInfo CalculateParkingFee(DateTime EnterTime, DateTime ExitTime, string StampCode, int SubStampID, int CardType_id, int CardLossCost, int CardDamagedCost)
    {
        FeeInfo result = new FeeInfo();
        try
        {
            int parkingFee = 0, discountFee = 0, discount_id = 0xff;

            result.PointDiscount = 0;

            DataRow[] dr_StampInfo = Config.dtStampInfo.Select("STAMP_CODE = '" + StampCode + "' and SUB_STAMP_ID = '" + SubStampID + "' and CARDTYPE_ID = '" + CardType_id + "' and active_id = 'Y'");

            string stampEmpty = AddStampCodeLength(0);

            if (dr_StampInfo.Length == 0)
            {
                // AddLog("ตราประทับผิดพลาด StampCode=" + StampCode + " CardType=" + CardType_id.ToString());
                // AddLog("ERROR STAMPCODE : " + StampCode + " CardType=" + CardType_id.ToString());
                dr_StampInfo = Config.dtStampInfo.Select("STAMP_CODE = '" + stampEmpty + "' and SUB_STAMP_ID = '0' and CARDTYPE_ID = '" + CardType_id + "' and active_id = 'Y'");

                if (dr_StampInfo.Length > 0)
                {
                    result.StampID = Convert.ToInt32(dr_StampInfo[0]["STAMP_ID"]);
                    result.StampCode = stampEmpty;
                    result.SubStampID = 0;
                }
                else
                {
                    dr_StampInfo = Config.dtStampInfo.Select("active_id = 'Y'");
                    result.StampID = Convert.ToInt32(dr_StampInfo[0]["STAMP_ID"]);
                }
            }
            else
            {
                result.StampCode = StampCode;
                result.StampID = Convert.ToInt32(dr_StampInfo[0]["STAMP_ID"]);
                result.SubStampID = SubStampID;
            }

            int discountMinutes = 0;
            int discountBaht = 0;
            discount_id = Convert.ToInt32(dr_StampInfo[0]["discount_id"]);
            DataRow[] dr_Discount = Config.dtDiscount.Select("id = '" + discount_id.ToString() + "' and active_id = 'Y'");

            if (dr_Discount.Length > 0)
            {
                discountMinutes = Convert.ToInt32(dr_Discount[0]["minute_discount"]);
                discountBaht = Convert.ToInt32(dr_Discount[0]["baht_discount"]);
            }

            result.FeeScheduleID = Convert.ToInt32(dr_StampInfo[0]["feeschedule_id"]);
            DataRow[] dr_ratepack = Config.dtPackrate.Select("FEESCHEDULE_ID='" + result.FeeScheduleID + "' AND ((TIME_START_H<='" + EnterTime.Hour + "' AND TIME_END_H>'" + EnterTime.Hour + "') OR (TIME_START_H=TIME_END_H))");

            if (dr_ratepack == null)
            {
                // AddLog("รายการคำนวนเงินผิดพลาด StampCode=" + StampCode + " CardType=" + CardType_id.ToString() + " FeeScheduleID=" + result.FeeScheduleID.ToString());
                // AddLog("ERROR RATEPACK STAMPCODE : " + StampCode + " CardType=" + CardType_id.ToString() + " FeeScheduleID=" + result.FeeScheduleID.ToString());
                dr_ratepack = Config.dtPackrate.Select("FEESCHEDULE_ID='1'");
            }
            if (dr_ratepack.Length == 0)
            {
                // AddLog("รายการคำนวนเงินผิดพลาด StampCode=" + StampCode + " CardType=" + CardType_id.ToString() + " FeeScheduleID=" + result.FeeScheduleID.ToString());
                // AddLog("ERROR RATEPACK STAMPCODE : " + StampCode + " CardType=" + CardType_id.ToString() + " FeeScheduleID=" + result.FeeScheduleID.ToString());
                dr_ratepack = Config.dtPackrate.Select("FEESCHEDULE_ID='1'");
            }

            DataTable dtRate = new DataTable();
            dtRate.Columns.Add("Hour", typeof(int));
            dtRate.Columns.Add("Start", typeof(DateTime));
            dtRate.Columns.Add("End", typeof(DateTime));
            dtRate.Columns.Add("Normal", typeof(int));
            dtRate.Columns.Add("Extra", typeof(int));
            dtRate.Columns.Add("Overnight", typeof(int));

            DataTable dtDiscount = dtRate.Clone();

            int totalMinutes = (int)((TimeSpan)(ExitTime - EnterTime)).TotalMinutes;
            int freeMinutes = Convert.ToInt32(dr_ratepack[0]["FREE_MINUTE"]);

            //lbDiscountMin.Text = "Discount minute : " + discountMinutes;
            //lbDiscountBaht.Text = "Discount baht : " + discountBaht;


            if (totalMinutes <= freeMinutes)
            {
                result.discount_minute = 0;
                result.ParkingFee = 0;
                result.StampDiscount = 0;
                result.CardLossFee = CardLossCost;
                result.CardDamagedFee = CardDamagedCost;
                result.TotalFee = CardLossCost + CardDamagedCost;
            }
            else
            {
                dtRate = TableFee(EnterTime, ExitTime, dr_ratepack);

                if (dtRate.Rows.Count > 0)
                {
                    parkingFee = Convert.ToInt32(dtRate.Compute("SUM(Normal)", ""))
                        + Convert.ToInt32(dtRate.Compute("SUM(Extra)", ""));
                    //+ Convert.ToInt32(dtRate.Compute("SUM(Overnight)", ""));
                }
                else
                {
                    parkingFee = 0;
                }

                //lbParkingFee.Text = "Parking Fee : " + parkingFee;

                if (discountMinutes == -1)
                {
                    discountFee = parkingFee;
                }
                else if (discountMinutes > 0)
                {
                    int feeAfterDiscount = 0;
                    dtDiscount = TableFee(EnterTime.AddMinutes(discountMinutes), ExitTime, dr_ratepack);

                    if (dtDiscount.Rows.Count > 0)
                    {
                        feeAfterDiscount = Convert.ToInt32(dtDiscount.Compute("SUM(Normal)", ""))
                        + Convert.ToInt32(dtDiscount.Compute("SUM(Extra)", ""));
                        //+ Convert.ToInt32(dtDiscount.Compute("SUM(Overnight)", ""));
                    }
                    else
                    {
                        feeAfterDiscount = 0;
                    }

                    discountFee = parkingFee - feeAfterDiscount;
                }

                discountFee += discountBaht;

                if (discountFee > parkingFee)
                    discountFee = parkingFee;

                int OvernightCost = 0;

                if (dtRate.Rows.Count > 0)
                {
                    OvernightCost = Convert.ToInt32(dtRate.Compute("SUM(Overnight)", ""));
                }

                result.Parking_Minute = totalMinutes;
                result.discount_baht = discountBaht;
                result.discount_minute = discountMinutes;
                result.ParkingFee = parkingFee + OvernightCost;
                result.StampDiscount = discountFee;
                result.Overnight = OvernightCost;
                result.CardLossFee = CardLossCost;
                result.CardDamagedFee = CardDamagedCost;
                result.TotalFee = (parkingFee - discountFee) + (OvernightCost + CardLossCost + CardDamagedCost);
            }

            if (result.TotalFee < 0)
                result.TotalFee = 0;
            return result;
        }
        catch (Exception ex)
        {
            // AddLog("CalculateParkingFee Exception : " + ex.Message);
            result.TotalFee = 0;
            return result;
        }
    }

    private static DataTable TableFee(DateTime EnterTime, DateTime ExitTime, DataRow[] dr_ratepack)
    {
        DataTable dtInfo = new DataTable();
        dtInfo.Columns.Add("Hour", typeof(int));
        dtInfo.Columns.Add("Start", typeof(DateTime));
        dtInfo.Columns.Add("End", typeof(DateTime));
        dtInfo.Columns.Add("Normal", typeof(int));
        dtInfo.Columns.Add("Extra", typeof(int));
        dtInfo.Columns.Add("Overnight", typeof(int));

        int i = 0, j = 0, last_rate = 0, last_extrarate = 0;
        bool firstday = true;

        int fraction_minute = Convert.ToInt32(dr_ratepack[0]["FRACTION_MINUTE"]);
        bool extra_charge = Convert.ToBoolean(dr_ratepack[0]["EXTRA_CHARGE"]);
        TimeSpan extra_start = (TimeSpan)(dr_ratepack[0]["EXTRA_START"]);

        DateTime dt_extrastart = EnterTime.Date + extra_start;
        if (extra_start >= new TimeSpan(0, 0, 0) && extra_start <= new TimeSpan(6, 0, 0))
        {
            if (dt_extrastart < EnterTime && EnterTime.TimeOfDay > new TimeSpan(6, 0, 0))
            {
                dt_extrastart = dt_extrastart.AddDays(1);
            }
        }
        else
        {
            if (EnterTime.TimeOfDay >= new TimeSpan(0, 0, 0) && EnterTime.TimeOfDay <= new TimeSpan(6, 0, 0))
            {
                dt_extrastart = dt_extrastart.AddDays(-1);
            }
        }

        TimeSpan extra_end = (TimeSpan)(dr_ratepack[0]["EXTRA_END"]);

        DateTime dt_extraend = dt_extrastart.Date + extra_end;
        if (dt_extraend <= dt_extrastart)
        {
            dt_extraend = dt_extraend.AddDays(1);
        }

        string[] packrate = dr_ratepack[0]["RATE"].ToString().Split(',');
        string[] extra_rate = dr_ratepack[0]["EXTRA_RATE"].ToString().Split(',');
        bool FeePlusExtra = Convert.ToBoolean(dr_ratepack[0]["FEE_PLUS_EXTRA"]);
        bool overnight_charge = Convert.ToBoolean(dr_ratepack[0]["OVERNIGHT_CHARGE"]);
        int overnight_rate = Convert.ToInt32(dr_ratepack[0]["OVERNIGHT_RATE"]);
        TimeSpan overnight_start = (TimeSpan)(dr_ratepack[0]["OVERNIGHT_START"]);
        TimeSpan overnight_end = (TimeSpan)(dr_ratepack[0]["OVERNIGHT_END"]);
        DateTime dt_overnightstart = EnterTime.Date + overnight_start;


        if (overnight_start == overnight_end)
        {
            if (dt_overnightstart < EnterTime)
            {
                dt_overnightstart = dt_overnightstart.AddDays(1);
            }
        }
        else
        {
            if (overnight_start >= new TimeSpan(0, 0, 0) && overnight_start <= new TimeSpan(6, 0, 0))
            {
                if (dt_overnightstart < EnterTime && EnterTime.TimeOfDay > new TimeSpan(6, 0, 0))
                {
                    dt_overnightstart = dt_overnightstart.AddDays(1);
                }
            }
            else
            {
                if (EnterTime.TimeOfDay >= new TimeSpan(0, 0, 0) && EnterTime.TimeOfDay <= new TimeSpan(6, 0, 0))
                {
                    dt_overnightstart = dt_overnightstart.AddDays(-1);
                }
            }
        }

        DateTime dt_overnightend = dt_overnightstart.Date + overnight_end;
        if (dt_overnightend < dt_overnightstart)
        {
            dt_overnightend = dt_overnightend.AddDays(1);
        }

        //lbFreeMin.Text = "Free minute : " + dr_ratepack[0]["FREE_MINUTE"].ToString();
        //lbFracMin.Text = "Fraction minute : " + dr_ratepack[0]["FRACTION_MINUTE"].ToString();
        //lbNormalRate.Text = "Normal rate : " + dr_ratepack[0]["RATE"].ToString();
        //lbExtraRate.Text = "Extra rate : " + dr_ratepack[0]["EXTRA_CHARGE"].ToString()
        //    + " | " + dr_ratepack[0]["EXTRA_RATE"].ToString()
        //    + " | " + dr_ratepack[0]["EXTRA_START"].ToString() + " - " + dr_ratepack[0]["EXTRA_END"].ToString();
        //lbOvernightRate.Text = "Overnight rate : " + dr_ratepack[0]["OVERNIGHT_CHARGE"].ToString()
        //    + " | " + dr_ratepack[0]["OVERNIGHT_RATE"].ToString();
        //if (overnight_start == overnight_end)
        //{
        //    lbOvernightRate.Text += " | " + dr_ratepack[0]["OVERNIGHT_START"].ToString();
        //}
        //else
        //{
        //    lbOvernightRate.Text += " | " + dr_ratepack[0]["OVERNIGHT_START"].ToString() + " - " + dr_ratepack[0]["OVERNIGHT_END"].ToString();
        //}

        //lbFeePlusExtra.Text = "Fee plus Extra : " + dr_ratepack[0]["FEE_PLUS_EXTRA"].ToString();

        DateTime running_time = new DateTime(EnterTime.Year, EnterTime.Month, EnterTime.Day, EnterTime.Hour, EnterTime.Minute, 0);
        DateTime endtime = new DateTime(ExitTime.Year, ExitTime.Month, ExitTime.Day, ExitTime.Hour, ExitTime.Minute, 0);

        int hour = 0;

        while (running_time < endtime)
        {
            int normal = 0;
            int extra = 0;
            int overnight = 0;

            DateTime endhour = running_time.AddHours(1);
            if (endhour > endtime)
            {
                endhour = endtime;
            }

            int minuteDiff = (endhour - running_time).Hours * 60 + (endhour - running_time).Minutes;

            if (minuteDiff >= fraction_minute)
            {
                if (i >= packrate.Length)
                {
                    normal = last_rate;
                }
                else
                {
                    normal = Convert.ToInt32(packrate[i]);
                    last_rate = Convert.ToInt32(packrate[i]);
                    i++;
                }

                if (extra_charge)
                {
                    if (firstday && EnterTime >= dt_extrastart && EnterTime <= dt_extraend)
                    {
                        if (endhour > dt_extraend.Date + new TimeSpan(dt_extraend.Hour, EnterTime.TimeOfDay.Minutes, 0))
                        {
                            dt_extrastart = dt_extrastart.AddDays(1);
                            dt_extraend = dt_extraend.AddDays(1);
                            firstday = false;
                        }
                    }
                    else
                    {
                        if (endhour > dt_extraend)
                        {
                            dt_extrastart = dt_extrastart.AddDays(1);
                            dt_extraend = dt_extraend.AddDays(1);
                        }
                    }

                    if (endhour > dt_extrastart)
                    {
                        if (j >= extra_rate.Length)
                        {
                            extra = last_extrarate;
                        }
                        else
                        {
                            extra = Convert.ToInt32(extra_rate[j]);
                            last_extrarate = Convert.ToInt32(extra_rate[j]);
                        }
                        j++;

                        if (!FeePlusExtra)
                        {
                            normal = 0;
                        }
                    }
                }
            }

            if (overnight_charge)
            {
                if (overnight_start == overnight_end)
                {
                    if (endhour >= dt_overnightstart)
                    {
                        overnight = overnight_rate;
                        dt_overnightstart = dt_overnightstart.AddDays(1);
                        dt_overnightend = dt_overnightend.AddDays(1);
                    }
                }
                else
                {
                    if ((running_time >= dt_overnightstart && running_time <= dt_overnightend) || (endhour >= dt_overnightstart && endhour <= dt_overnightend))
                    {
                        overnight = overnight_rate;
                        dt_overnightstart = dt_overnightstart.AddDays(1);
                        dt_overnightend = dt_overnightend.AddDays(1);
                    }
                }
            }

            hour++;
            dtInfo.Rows.Add(new object[] { hour, running_time, endhour, normal, extra, overnight });
            running_time = running_time.AddHours(1);
        }

        return dtInfo;
    }

    private static string AddStampCodeLength(int _code)
    {
        string stampCodeLength = "D5";
        return _code.ToString(stampCodeLength);
    }
}

