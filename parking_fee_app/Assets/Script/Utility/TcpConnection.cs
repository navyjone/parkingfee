﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//using System.Diagnostics;
using System.IO;
using UnityEngine;
using Unity.IO.Compression;

//using UnityEngine;

//using InfinteEncryption;


public class TcpConnection
{
    public byte[] sending_buffer = new byte[100000];
    const int PACKSIZE = 1400;
    public static Socket client = null;

    public class Code
    {
        public const byte STX = 0x02;
        public const byte EOT = 0x04;
        public const byte ACK = 0x06;
        public const byte NACK = 0x15;

        public const byte CONNECT = 0x15;

        public const byte reqUserInfo = 0xA0;
        public const byte reqParkingCard = 0xA1;
        public const byte reqCardType = 0xA2;
        public const byte reqCardGroup = 0xA3;
        public const byte reqBlackList = 0xA4;
        public const byte reqProvince = 0xA5;
        public const byte reqSystemInfo = 0xA6;
        public const byte reqpackrate = 0xA7;
        public const byte reqparkingrate = 0xA8;
        public const byte reqstampCode = 0xA9;
        public const byte checkcardloss = 0xAA;
        public const byte reqgateName = 0xAB;
        public const byte reqcardloss = 0xAC;
        public const byte reqOptioncost = 0xAD;
        public const byte reqStampInfo = 0xAE;
        public const byte reqDiscount = 0xAF;

        public const byte reqFeeSchedule = 0xB0;
        public const byte reqSubStamp = 0xB1;
        public const byte reqServerTime = 0xB2;
        public const byte reqParkingLot = 0xB3;
        public const byte reqParkingCardMap = 0xB4;
        public const byte reqSetting = 0xB5;
        public const byte reqCashcharge = 0xB6;
    }

    public class Status
    {
        public const string connectFail = "connectFail";
        public const string success = "success";
        public const string responseWrongFormat = "responseWrongFormat";
        public const string timeout = "timeout";
        public const string decompressFail = "decompressFail";
    }

    //obsolete
    /*
    public static void StartClient()
    {
        client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        //IPServer = Dns.GetHostEntry("Innet-rd.no-ip.biz").AddressList[0];
        //IPServer = IPAddress.Parse("127.0.0.1");
        IPServer = IPAddress.Parse("192.168.100.4");
        //IPServer = IPAddress.Parse("103.253.73.86");
        serverEP = new IPEndPoint(IPServer, port);
    }

    public static bool connect_server()
    {
        if (!client.Connected)
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.Connect(serverEP);
            for (int i = 0; i < 40; i++)
            {
                Thread.Sleep(50);
                if (client.Connected)
                    return (true);
            }
            return (false);
        }
        return true;
    }
    */
    private static Thread thread;

    static void RequestAsync(byte cmd)
    {
    }

    public static string request(byte cmd)
    {
        //IPAddress serverIP = IPAddress.Parse("103.253.73.86");
        //IPEndPoint ipEnd = new IPEndPoint(serverIP, 9789); // 9789
        string targetIP = Config.Get(Constant.serverIP);
        IPAddress serverIP = IPAddress.Parse(targetIP);
        IPEndPoint ipEnd = new IPEndPoint(serverIP, 9789); // 9789
        byte[] sBuf = new byte[1500];
        int index = 0;

        DateTime lastData = DateTime.Now;

        byte[] Buffer = new byte[3000];
        int iRx = 0, frameLen = 0;
        byte chksum = 0x00;
        int fIndex = 0, fSize = 0, packId = 0, bReceived = 0;


        string flName = "";
        string filePath = Constant.systemPath + "/XML";

        byte[] fBuf = null;
        DateTime sTime = DateTime.Now;
        DateTime cTime = DateTime.Now;

        FileStream fs = null;
        BinaryWriter w = null;

        try
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.Connect(ipEnd);
            if (!client.Connected)
                return (Status.connectFail);
        }
        catch
        {
            return (Status.connectFail);
        }

        try
        {
            // 0  1  2     3      4         5     6...
            //stx-bh-bl-gateId-cmd1byte-cmd_info-sum-eot
            sBuf[0] = 0x02;
            sBuf[3] = 0;
            sBuf[4] = cmd;
            index = 5;
            sBuf[1] = (byte)(((index - 3) >> 8) & 0xff);
            sBuf[2] = (byte)(((index - 3)) & 0xFF);

            chksum = 0x00;
            for (int i = 1; i < index; i++)
            {
                chksum ^= sBuf[i];
            }
            sBuf[index++] = chksum;
            sBuf[index++] = 0x04;
            client.Send(sBuf, index, SocketFlags.None);

            #region Receivefile
            while (true)
            {

                if (client.Available > 0)
                {
                    //Debug.Log("client.Available");
                    iRx = client.Receive(Buffer, PACKSIZE + 10, 0);

                    frameLen = Buffer[1] * 256 + Buffer[2];
                    chksum = 0x00;
                    for (int i = 1; i < iRx - 2; i++)
                        chksum ^= Buffer[i];

                    if ((Buffer[0] == 0x02) && (frameLen == iRx - 5) && (Buffer[iRx - 2] == chksum) && (Buffer[iRx - 1] == 0x04))
                    {
                        sTime = DateTime.Now;
                        if (Buffer[3] == 0x00)
                        {
                            //Header
                            //stx-bh-bl-cmd-fileSize4bytes-fileName-sum-eot
                            flName = "";

                            for (int i = 0; i < frameLen - 5; i++)
                                flName += Convert.ToChar(Buffer[i + 8]).ToString();

                            fSize = Convert.ToInt32((Buffer[4] << 24) | (Buffer[5] << 16) | (Buffer[6] << 8) | Buffer[7]);
                            fBuf = new byte[fSize];
                            fIndex = 0;
                        }
                        else
                        {
                            //File Content
                            //data 1 packe : STX,framLen2byte,CMD,packId4byte,data1024byte-sum-EOT

                            packId = Convert.ToInt32((Buffer[4] << 24) | (Buffer[5] << 16) | (Buffer[6] << 8) | Buffer[7]);
                            bReceived += frameLen - 5;
                            Array.Copy(Buffer, 8, fBuf, fIndex, frameLen - 5);
                            fIndex = PACKSIZE * packId + frameLen - 5;

                            if (bReceived == fSize)
                            {
                                if (File.Exists(filePath + "/" + flName))
                                {
                                    File.Delete(filePath + "/" + flName);
                                    Thread.Sleep(100);
                                    fs = new FileStream(filePath + "/" + flName, FileMode.CreateNew, FileAccess.Write);
                                    w = new BinaryWriter(fs);
                                }
                                else
                                {
                                    fs = new FileStream(filePath + "/" + flName, FileMode.CreateNew);
                                    w = new BinaryWriter(fs);
                                }

                                w.Write(fBuf, 0, fSize);
                                w.Close();
                                fs.Close();

                                try
                                {
                                    string recevedFile = Decompress(new FileInfo(filePath + "/" + flName));
                                    //Debug.Log("Decompress recevedFile = " + recevedFile);
                                    return Status.success;
                                }
                                catch(Exception ex)
                                {
                                    return Status.decompressFail + "  " +ex.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        //for(int i = 0; i < 40; i++)
                        //{
                        //    Debug.Log(Buffer[i].ToString("X2") + ", ");
                        //}
                        w.Close();
                        fs.Close();
                        return Status.responseWrongFormat;
                    }

                    Thread.Sleep(200);
                }

                if ((DateTime.Now - sTime).TotalSeconds >   5)
                {
                    w.Close();
                    fs.Close();
                    return Status.timeout;
                }
            }//end while(true)
            #endregion
        }
        catch (Exception ec)
        {
            //Debug.LogError(ec.Message);
        }
        finally
        {
        }

        return Status.responseWrongFormat;
    }



    private static string Decompress(FileInfo fileToDecompress)
    {
        using (FileStream originalFileStream = fileToDecompress.OpenRead())
        {
            string currentFileName = fileToDecompress.FullName;
            string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

            using (FileStream decompressedFileStream = File.Create(newFileName))
            {
                using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                {
                    CopyTo(decompressionStream, decompressedFileStream);
                    return newFileName;
                }
            }
        }
    }

    private static long CopyTo(Stream source, Stream destination)
    {
        byte[] buffer = new byte[2048];
        int bytesRead;
        long totalBytes = 0;
        while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
        {
            destination.Write(buffer, 0, bytesRead);
            totalBytes += bytesRead;
        }
        return totalBytes;
    }
}

