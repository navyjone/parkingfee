﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToastrControl : MonoBehaviour {

    public GameObject backgroundObject;
    public GameObject messageObject;

    public float fadeTime = 0.01f;
    public float showTime = 5f;

    public static string toastrMessage;
    public static bool toastrShow;
    public static bool toastrHide;
    public static Color32 toastrBackground;
    public static Color32 toastrSuccess = new Color32(54, 204, 20, 0);
    public static Color32 toastrError = new Color32(240, 76, 76, 0);
    public static Color32 toastrWarning = new Color32(255, 132, 0, 0);

    private Image background;
    private Text message;

    private Color targetBackgroundColor;
    private Color targetTextColor;
    private bool showFlag;
    private bool hideFlag;

    // Use this for initialization
    void Start () {
        background = backgroundObject.GetComponent<Image>();
        message = messageObject.GetComponent<Text>();

    }

    void Update ()
    {
        if (toastrShow)
        {
            Show();
            toastrShow = false;
            background.color = (Color)toastrBackground;
            message.text = toastrMessage;
        }

        if (toastrHide)
        {
            Hide();
            toastrHide = false;
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (showFlag)
        {
            targetBackgroundColor = new Color(background.color.r,
                                                background.color.g,
                                                background.color.b, 
                                                1);
            background.color = Color.Lerp(background.color, targetBackgroundColor, fadeTime);

            targetTextColor = new Color(message.color.r,
                                    message.color.g,
                                    message.color.b,
                                    1);
            message.color = Color.Lerp(message.color, targetTextColor, fadeTime);
        }
        
        if (hideFlag)
        {
            targetBackgroundColor = new Color(background.color.r,
                                                background.color.g,
                                                background.color.b,
                                                0);
            background.color = Color.Lerp(background.color, targetBackgroundColor, fadeTime);

            targetTextColor = new Color(message.color.r,
                                    message.color.g,
                                    message.color.b,
                                    0);
            message.color = Color.Lerp(message.color, targetTextColor, fadeTime);

            if (message.color.a < 0.01)
            {
                hideFlag = false;
                backgroundObject.SetActive(false);
                messageObject.SetActive(false);
            }
        }
	}

    IEnumerator WaitTime ()
    {
        yield return new WaitForSeconds(showTime);
        showFlag = false;
        hideFlag = true;
    }

    public void Show()
    {
        backgroundObject.SetActive(true);
        messageObject.SetActive(true);
        showFlag = true;
        hideFlag = false;
        StartCoroutine(WaitTime());
    }

    public void Hide()
    {
        showFlag = false;
        hideFlag = true;

    }
}
