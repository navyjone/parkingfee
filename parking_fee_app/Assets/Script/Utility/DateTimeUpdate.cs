﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DateTimeUpdate : MonoBehaviour {
	public Text TimeTx;
	public Text DateTx;
	// Use this for initialization
	void Start () {
		this.SetDateTime();
	}
	
	// Update is called once per frame
	void Update () {
		this.SetDateTime();
	}

	private void SetDateTime () 
	{
		TimeTx.text = DateTime.Now.ToString("HH:mm");
		DateTx.text = DateTime.Now.ToString("dd/MM/yyyy");
	}
}
