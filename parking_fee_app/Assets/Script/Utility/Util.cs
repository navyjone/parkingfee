﻿using UnityEngine;
using System.Collections;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

public class Util {

    public static string GetShiftId()
    {
        try
        {
            var gateId = Config.Get(Constant.exitGateId);
            DateTime loginDateTime = DateTime.Parse(Config.Get(Constant.lastLogin));
            string yearShift = (loginDateTime.Year - 2000).ToString();
            string result = int.Parse(gateId).ToString("D2") + yearShift + loginDateTime.Month.ToString("D2") + loginDateTime.Day.ToString("D2")
                + loginDateTime.Hour.ToString("D2") + loginDateTime.Minute.ToString("D2");
            return result;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public static string GetCashChargeId()
    {
        try
        {
            string result = "";
            string gateId = Config.Get(Constant.exitGateId);
            DataRow[] rows = Config.dtCashCharge.Select("GATE_ID = " + gateId);
            if (rows.Length > 0)
            {
                result = rows[0]["CASHCHARGE_ID"].ToString();
            }

            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void WriteTextFile(string path, string fileName, string message)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        using (StreamWriter writer = File.AppendText(path + fileName))
        {
            writer.Write(message);
        }
    }

    public static string GetHeaderSlip()
    {
        try
        {
            string result = "";
            DataRow[] rows = Config.dtSetting.Select("SETTING_NAME = 'HeaderSlipText'");
            result = rows[0]["SETTING_VALUE"].ToString();
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static bool IsAllowAccessGate(string uid, string inputGateId)
    {
        try
        {
            System.Collections.Generic.List<string> gateIds = CardData.GetGatePass(uid);
            if (gateIds.Count == 0) return false;
            var foundGateId = gateIds.Where(g => g == inputGateId).FirstOrDefault();
            if (!string.IsNullOrEmpty(foundGateId))
            {
                return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static byte[] ConvertStringToByte(string input)
    {
        string[] inputAfterSplit = input.Split(',');
        string hex = "";
        for (int i = 0; i < inputAfterSplit.Length; i++)
        {
            hex += inputAfterSplit[i];
        }

        return ToByteArray(hex);
    }

    public static byte[] ToByteArray(string HexString)
    {
        int NumberChars = HexString.Length;
        byte[] bytes = new byte[NumberChars / 2];
        for (int i = 0; i < NumberChars; i += 2)
        {
            //bytes[i / 2] = Convert.ToByte(HexString.Substring(i, 2), 16);
            bytes[i / 2] = (byte)Convert.ToInt32(HexString.Substring(i, 2), 16);
            //int intBuf = Convert.ToInt32(HexString.Substring(i, 2), 16);
        }
        return bytes;
    }

    public static string ByteArrayToHexStringWithSelectBlock(byte[] data, int index)
    {
        StringBuilder sb = new StringBuilder(data.Length * 2);
        foreach (byte b in data)
        {
            sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
        }
        return (sb.ToString().ToUpper()).Substring(index * 32, 32);
    }

    public static string ByteArrayToHexString(byte[] data)
    {
        StringBuilder hex = new StringBuilder(data.Length * 2);
        foreach (byte b in data)
            hex.AppendFormat("{0:x2}", b);
        return hex.ToString();
    }
}
