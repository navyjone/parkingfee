﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class PrinterZKC
{
    public static string Init()
    {
        try
        {
            var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var unityActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            ajc.CallStatic("ReceiveActivityInstance", unityActivity);
            var result = ajc.CallStatic<string>("Init");
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            throw ex;
        }
    }

    public static string GetStatus()
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            string result = ajc.CallStatic<string>("GetStatus");
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            return null;
        }
    }

    public static string GetFirmware()
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            string result = ajc.CallStatic<string>("GetFirmware");
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            return null;
        }
    }

    public static void PrintShift(PrintData.ShiftInput Shift)
    {
        try
        {
            //CommonFn.callLoading(true);
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            //PrintTextAlgin(String data, int algin, int size)
            ajc.CallStatic<string>("PrintTextAlgin", Shift.siteName + "\n\n\n", 1, 0);
            var body = "-------------------------------" +
                        " CASHIER SUMMARY \n" +
                        "POS ID. : " + Shift.cashierNumber + "\n" +
                        "SHIFT ID. : " + Shift.shiftId + "\n" +
                        "IN : " + Shift.LoginTime + "\n" +
                        "OUT: " + Shift.LogoutTime + "\n" +
                        "-------------------------------" +
                        " TOTAL CAR: " + Shift.totalCar + "\n" +
                        "TOTAL AMOUNT: " + Shift.totalAmount + "\n";
            ajc.CallStatic<string>("PrintUnicode", body);
            ajc.CallStatic<string>("PrintUnicode", "\n\n\n\n\n");
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }

    public static void PrintExit(PrintData.ExitGateInput ExitGate)
    {
        try
        {
            //CommonFn.callLoading(true);
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            //PrintTextAlgin(String data, int algin, int size)
            //ajc.CallStatic<string>("PrintTextAlgin", $"{ExitGate.siteName}\n\n", 1, 0);

            //var body = $"TM. : {ExitGate.gateName}\n" +
            //            $"TAX ID. : {ExitGate.taxId}\n" +
            //            $"POS ID. : {ExitGate.posId}\n" +
            //            $"Rcpt ID. : {ExitGate.receiptId}\n" +
            //            $"LICENSE PLATE : {ExitGate.license}\n" +
            //            $"IN : {ExitGate.enterDate}\n" +
            //            $"OUT: {ExitGate.exitDate}\n" +
            //            $"--------------------------------\n";
            ajc.CallStatic<string>("PrintTextAlgin", ExitGate.siteName + "\n\n", 1, 0);

            var body = "TM. : " + ExitGate.gateName + "\n" +
                       "TAX ID. : " + ExitGate.taxId + "\n" +
                       "POS ID. : " + ExitGate.posId + "\n" +
                       "Rcpt ID. : " + ExitGate.receiptId + "\n" +
                       "LICENSE PLATE : " + ExitGate.license + "\n" +
                       "IN : " + ExitGate.enterDate + "\n" +
                       "OUT: " + ExitGate.exitDate + "\n" +
                       "--------------------------------\n";
            string range = "{0,-17} {1,14}";
            Debug.Log("Config.GetDevice(Constant.isReceiptDisplayDiscountLine) = " + Config.Get(Constant.isReceiptDisplayDiscountLine));
            var isPrintDiscountLine = bool.Parse(Config.Get(Constant.isReceiptDisplayDiscountLine));
            var parkingFee = ExitGate.parkingFee;
            if (isPrintDiscountLine)
            {
                body += String.Format(range, "Parking Fee", parkingFee + " Baht") + "\n";
                body += String.Format(range, "Stamp " + ExitGate.stampCode, "-" + ExitGate.stampDiscount + " Baht") + "\n";
            }
            else
            {
                parkingFee -= ExitGate.stampDiscount;
                body += String.Format(range, "Parking Fee", parkingFee + " Baht") + "\n";
            }

            body += "--------------------------------\n" +
                    String.Format(range, "Cash Received", parkingFee + " Baht") + "\n" +
                    "--------------------------------\n\n";
            ajc.CallStatic<string>("PrintUnicode", body);
            ajc.CallStatic<string>("PrintTextAlgin", "VAT. INCLUDED\n\n", 1, 0);
            ajc.CallStatic<string>("PrintUnicode", "\n\n\n\n\n");
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }

    public static string PrintTest()
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            var result = ajc.CallStatic<string>("PrintUnicode", "13245 asdf\n");
            result = ajc.CallStatic<string>("PrintTextAlgin", "text algin 0\n", 0, 0);
            result = ajc.CallStatic<string>("PrintTextAlgin", "text algin 1 \n", 1, 0);
            result = ajc.CallStatic<string>("PrintTextAlgin", "text algin 2\n", 2, 0);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            return null;
        }
    }

    public static string PrintTest1()
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            var body = "row1 Na ka\n";
            SetAlignment(1);
            body += "row2 na ka\n";
            SetAlignment(2);
            var result = ajc.CallStatic<string>("PrintUnicode", body);
            //result paper = row1
            //               row2
            //                              row1
            //                              row2
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            return null;
        }
    }

    public static void SetFont(int size)
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            ajc.CallStatic<string>("SetFontSize", size);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }

    public static void SetAlignment(int align)
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            ajc.CallStatic<string>("SetAlignment", align);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }
}

