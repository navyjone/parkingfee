﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Message : MonoBehaviour {

	public GameObject messageCanvas;

	public static string headerText;
	public static string bodyText;
	public static string button1Text;
	public static string button2Text;
	public static Action button1Action;
	public static Action button2Action;

	public Text header;
	public Text body;
	public Text button1;
	public Text button2;

	void Start ()
	{
		header.text = headerText;
		body.text = bodyText;
		button1.text = button1Text;
		try
		{
			if (button2Text != null)
			{
				button2.text = button2Text;
			}
		}
		catch
		{
			Debug.LogWarning("fix it later button2 crash");
		}
	}
	

	public void Button1Action ()
	{
		try
		{
			button1Action();
		}
		catch(UnityException e)
		{
			Debug.Log(e);
		}
		Destroy(messageCanvas);
	}

	public void Button2Action ()
	{
		try
		{
			button2Action();
		}
		catch(UnityException e)
		{
			Debug.Log(e);
		}
		Destroy(messageCanvas);
	}

}
