﻿using UnityEngine;
using System.Collections;
using System;

public class Constant
{
    //public static string pathJavaObjReader = "com.example.starprinterandmifare.UnityPlayerActivity";
    public static string pathJavaObjReader = "navyjone.mifareclassic.Main";
    public static string pathJavaObjPrinter = "Navyjone.PrinterBLE.Main";
    public static string pathJavaObjPrinterZKC = "navyjone.printerzkc.Printer";
    // scene
    public const  string ExitGate = "ExitGate";
    public const string EnterGate = "EnterGate";
    public const string Login = "Login";
    public const string Setting = "Setting";
    public const string SelectMode = "SelectMode";
#if UNITY_EDITOR
    public static string systemPath = Application.dataPath;
#elif UNITY_ANDROID
    public static string systemPath = Application.persistentDataPath;
#endif
    //deviceInfo xml key
    public static string serverIP = "serverIP";
    public static string enterGateId = "enterGateId";
    public static string exitGateId = "exitGateId";
    public static string repceiptPrefix = "repceiptPrefix";
    public static string receiptNumber = "receiptNumber";
    public static string lastSync = "lastSync";
    public static string activePrinter = "activePrinter";
    public static string activeUserId = "activeUserId";
    public static string activeGateId = "activeGateId";
    public static string lastLogin = "lastLogin";
    public static string apiUrl = "apiUrl";
    public static string isReceiptDisplayDiscountLine = "isReceiptDisplayDiscountLine";

    //password
    public static byte[] passINNET = new byte[] { 0x49, 0x4E, 0x4E, 0x45, 0x54, 0x39 };
    public static byte[] passDefault = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

    //systeminfo xml key
    public static string address = "ADDRESS";
    public static string siteName = "NAME_TH";
    public static string vat = "VAT_RATE";
    public static string taxId = "TAX_ID";

    //XML Path
    public static string XMLPath = systemPath + "/XML/";
    public static string stampinfoXmlPath = XMLPath + "stampinfo.xml";
    public static string discountXmlPath = XMLPath + "discount.xml";
    public static string packrateXmlPath = XMLPath + "packrate.xml";
    public static string cardtypeXmlPath = XMLPath + "cardtype.xml";
    public static string userXmlPath = XMLPath + "userinfo.xml";
    public static string gateNameXmlPath = XMLPath + "gatename.xml";
    public static string settingXmlPath = XMLPath + "setting.xml";
    public static string systemInfoXmlPath = XMLPath + "systeminfo.xml";
    public static string deviceInfoXmlPath = XMLPath + "deviceinfo.xml";
    public static string cashChargeXmlPath = XMLPath + "cashcharge.xml";
    public static string parkingCardXmlPath = XMLPath + "parkingcard.xml";
    public static string parkingCardMapXmlPath = XMLPath + "parkingcardmap.xml";

    //bool at setting info datatable
    public static string TRUE = "TRUE";
    public static string FALSE = "FALSE";

    //printer response
    public static string printSuccess = "SUCCESS";
    public static string printerOnline = "ONLINE";

    //Sector mifare
    public static int infoSector = 8;

    //Logpath
    public static string logSuccessExitPath = Constant.systemPath + "/Log/Exit/Success/";
    public static string logFailExitPath = Constant.systemPath + "/Log/Exit/Fail/";
    public static string logSuccessEnterPath = Constant.systemPath + "/Log/Enter/Success/";
    public static string logFailEnterPath = Constant.systemPath + "/Log/Enter/Fail/";
    public static string logSuccessLoginPath = Constant.systemPath + "/Log/Login/Success/";
    public static string logFailLoginPath = Constant.systemPath + "/Log/Login/Fail/";
    public static string logShiftPath = Constant.systemPath + "/Log/Shift/";

    //Setting Panel
    public static string deviceSetting = "deviceSetting";
    public static string printerSetting = "printerSetting";
    public static string serverSetting = "serverSetting";
    public static string transactionSetting = "transactionSetting";

    public static class CardGroup
    {
        public const string visitor = "1";
        public const string member = "2"; 
    }

    public static string mifareClassic = "0";
}
