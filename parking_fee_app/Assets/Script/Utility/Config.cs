﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;


public class Config
{
    public static DataTable dtStampInfo = new DataTable();
    public static DataTable dtDiscount = new DataTable();
    public static DataTable dtPackrate = new DataTable();
    public static DataTable dtCardtype = new DataTable();
    public static DataTable dtUser = new DataTable();
    public static DataTable dtGateName = new DataTable();
    public static DataTable dtSetting = new DataTable();
    public static DataTable dtSysteminfo = new DataTable();
    public static DataTable dtCashCharge = new DataTable();
    public static DataTable dtParkingCard = new DataTable();
    public static DataTable dtParkingCardMap = new DataTable();
    //nvj xml
    //public static DataTable dtDeviceinfo = new DataTable();

    public static void LoadXmlToDataTable()
    {  
        try
        {
            dtStampInfo.Clear();
            dtStampInfo.TableName = "tb_stampinfo";
            dtStampInfo.ReadXml(Constant.stampinfoXmlPath);
            dtDiscount.Clear();
            dtDiscount.TableName = "tb_discount";
            dtDiscount.ReadXml(Constant.discountXmlPath);
            dtPackrate.Clear();
            dtPackrate.TableName = "tb_packrate";
            dtPackrate.ReadXml(Constant.packrateXmlPath);
            dtCardtype.Clear();
            dtCardtype.TableName = "tb_cardtype";
            dtCardtype.ReadXml(Constant.cardtypeXmlPath);
            dtUser.Clear();
            dtUser.TableName = "tb_userinfo";
            dtUser.ReadXml(Constant.userXmlPath);
            dtGateName.Clear();
            dtGateName.TableName = "tb_gatename";
            dtGateName.ReadXml(Constant.gateNameXmlPath);
            dtSetting.Clear();
            dtSetting.TableName = "tb_setting";
            dtSetting.ReadXml(Constant.settingXmlPath);
            dtSysteminfo.Clear();
            dtSysteminfo.TableName = "tb_systeminfo";
            dtSysteminfo.ReadXml(Constant.systemInfoXmlPath);
            dtCashCharge.TableName = "tb_cashcharge";
            dtCashCharge.ReadXml(Constant.cashChargeXmlPath);
            dtParkingCard.TableName = "tb_parkingcard";
            dtParkingCard.ReadXml(Constant.parkingCardXmlPath);
            dtParkingCardMap.TableName = "tb_parkingcardmap";
            dtParkingCardMap.ReadXml(Constant.parkingCardMapXmlPath);
            //dtDeviceinfo.Clear();
            //dtDeviceinfo.TableName = "deviceInfo";
            //dtDeviceinfo.ReadXml(deviceInfoXmlPath);
        }
        catch (Exception ex)
        {
            Debug.LogError("LoadXmlToDataTable " + ex.ToString());
        }
    }

    public static string RequestAll()
    {
        try
        {
            int retryCounter = 2;
            int successCounter = 0;
            Dictionary<byte, bool> requestDict = new Dictionary<byte, bool>
            {
                { TcpConnection.Code.reqpackrate, false },
                { TcpConnection.Code.reqStampInfo, false },
                { TcpConnection.Code.reqDiscount, false },
                { TcpConnection.Code.reqgateName, false },
                { TcpConnection.Code.reqCardType, false },
                { TcpConnection.Code.reqSystemInfo, false },
                { TcpConnection.Code.reqUserInfo, false },
                { TcpConnection.Code.reqSetting, false },
                { TcpConnection.Code.reqCashcharge, false },
                { TcpConnection.Code.reqParkingCard, false },
                { TcpConnection.Code.reqParkingCardMap, false }
            };

            successCounter = 0;

            while (retryCounter >= 0)
            {
                List<byte> keys = new List<byte>(requestDict.Keys);
                foreach (var key in keys)
                {
                    if (!requestDict[key])
                    {
                        var result = TcpConnection.request(key);
                        if (result == TcpConnection.Status.success)
                        {
                            Debug.Log("success " + key.ToString("X2"));
                            requestDict[key] = true;
                            successCounter++;
                        }
                        else
                        {
                            Debug.LogError("request " + key.ToString("X2") + " error because " + result);
                        }
                    }

                    Debug.Log("successCounter = " + successCounter);

                    if (successCounter >= requestDict.Count)
                    {
                        Debug.Log("all request success");
                        retryCounter = -1;
                        return "success";
                    }

                    System.Threading.Thread.Sleep(50);
                }

                retryCounter--;
            }

            LoadXmlToDataTable();
            return "";
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            return "";
        }
    }
    //public static void RequestAll()
    //{
    //    try
    //    {
    //        int retryCounter = 2;
    //        int successCounter = 0;
    //        Dictionary<byte, bool> requestDict = new Dictionary<byte, bool>();
    //        requestDict.Add(TcpConnection.Code.reqpackrate, false);
    //        requestDict.Add(TcpConnection.Code.reqStampInfo, false);
    //        requestDict.Add(TcpConnection.Code.reqDiscount, false);
    //        requestDict.Add(TcpConnection.Code.reqgateName, false);
    //        requestDict.Add(TcpConnection.Code.reqCardType, false);
    //        requestDict.Add(TcpConnection.Code.reqSystemInfo, false);
    //        requestDict.Add(TcpConnection.Code.reqUserInfo, false);
    //        requestDict.Add(TcpConnection.Code.reqSetting, false);
    //        requestDict.Add(TcpConnection.Code.reqCashcharge, false);
    //        requestDict.Add(TcpConnection.Code.reqParkingCard, false);
    //        successCounter = 0;

    //        while (retryCounter >= 0)
    //        {
    //            List<byte> keys = new List<byte>(requestDict.Keys);
    //            foreach (var key in keys)
    //            {
    //                if (!requestDict[key])
    //                {
    //                    var result = TcpConnection.request(key);
    //                    if (result == TcpConnection.Status.success)
    //                    {
    //                        Debug.Log("success " + key.ToString("X2"));
    //                        requestDict[key] = true;
    //                        successCounter++;
    //                    }
    //                    else
    //                    {
    //                        Debug.LogError("request " + key.ToString("X2") + " error because " + result);
    //                    }
    //                }

    //                if (successCounter >= requestDict.Count)
    //                {
    //                    Debug.Log("all request success");
    //                    retryCounter = -1;
    //                }

    //                System.Threading.Thread.Sleep(50);
    //            }

    //            retryCounter--;
    //        }

    //        LoadXmlToDataTable();
    //    }
    //    catch(Exception ex)
    //    {
    //        Debug.LogError(ex.ToString());
    //        return "";
    //    }
    //}

    public static string Get(string key)
    {
        try
        {
            string result = "";
            XDocument xDoc = XDocument.Load(Constant.deviceInfoXmlPath);
            var element = xDoc.Root.Element(key);
            if (element != null)
            {
                result = element.Value;
            }

            return result;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public static void Save(Dictionary<string, string> configInputs)
    {
        try
        {
            XDocument xDoc = XDocument.Load(Constant.deviceInfoXmlPath);
            foreach (var config in configInputs)
            {
                if (xDoc.Root.Element(config.Key) != null)
                {
                    xDoc.Root.Element(config.Key).Value = config.Value;
                }
            }

            xDoc.Save(Constant.deviceInfoXmlPath);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public static void InitialDevice ()
    {
        try
        {
            if (!Directory.Exists(Constant.XMLPath))
            {
                Directory.CreateDirectory(Constant.XMLPath);
            }

            //TODO PARKFEE-2
            if (!File.Exists(Constant.deviceInfoXmlPath))
            {
                // if file not exist create default one
                new XDocument(
                    new XElement("root",
                        new XElement(Constant.serverIP, "192.168.100.4"),
                        //new XElement(Constant.serverIP, "103.253.73.86"),
                        new XElement(Constant.enterGateId, "1"),
                        new XElement(Constant.exitGateId, "2"),
                        new XElement(Constant.repceiptPrefix, "2018-01-01"),
                        new XElement(Constant.receiptNumber, "1"),
                        new XElement(Constant.activePrinter, "BT:00:15:0E:E5:7E:24"),
                        new XElement(Constant.activeUserId, ""),
                        new XElement(Constant.activeGateId, ""),
                        new XElement(Constant.lastLogin, ""),
                        new XElement(Constant.lastSync, ""),
                        new XElement(Constant.apiUrl, "http://navyjone.com/PMSInnet/api/"),
                        new XElement(Constant.isReceiptDisplayDiscountLine, "false")
                    )
                ).Save(Constant.deviceInfoXmlPath);
            }
        }
        catch(Exception ex)
        {
            Debug.LogError("init device fail = " + ex.ToString());
        }
    }
}


