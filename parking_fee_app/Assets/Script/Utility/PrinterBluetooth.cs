﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public static class PrinterBluetooth
{
    public struct Input
    {
        public string siteName;
        public string taxId;
        public string posId;
        public string receiptId;
        public string license;
        public string enterDate;
        public string exitDate;
        public int parkingFee;
        public int penaltyFee;
        public string stampCode;
        public int stampDiscount;
        public int couponDiscount;
        public int cashReceive;
        public string gateName;
        public string address;
        public string branch;
    }

    public struct ShiftInput
    {
        public string siteName;
        public string totalCar;
        public string cashierNumber;
        public string LoginTime;
        public string LogoutTime;
        public string shiftId;
        public string totalAmount;
    }

    public static void List(AndroidJavaObject activityContext)
    {
        try
        {
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
                AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
                string result = ajc.CallStatic<string>("List");
                GameObject Setting = GameObject.Find("Setting/PrinterPanel");
                ((PrinterSetting)Setting.GetComponent(typeof(PrinterSetting))).ListPrinterCallback(result);
            }));
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    public static string GetStatus(string portName)
    {
        try
        {
            //sample name "BT:00:15:0E:E5:7E:24";
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
            string result = ajc.CallStatic<string>("Status", portName);
            return result;
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
            return null;
        }
    }

    public static void GetStatusDeletgate(string portName, string scene, AndroidJavaObject activityContext)
    {
        try
        {
            //sample name "BT:00:15:0E:E5:7E:24";
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
                try
                {
                    string result = ajc.CallStatic<string>("Status", portName);
                    GameObject MainEvent = GameObject.Find("_MainEvent");
                    if (result == Constant.printerOnline)
                    {
                        if (scene == Constant.ExitGate)
                        {
                            ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).ChangeColorPrinterStatusIcon(true);
                        }
                        else if (scene == Constant.EnterGate)
                        {

                        }
                    }
                    else
                    {
                        if (scene == Constant.ExitGate)
                        {
                            ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).ChangeColorPrinterStatusIcon(false);
                        }
                        else if (scene == Constant.EnterGate)
                        {

                        }
                    }
                }
                catch(Exception ex)
                {
                    Debug.LogError("at GetStatusDeletgate callback " + ex.ToString());
                    GameObject MainEvent = GameObject.Find("_MainEvent");
                    ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).ChangeColorPrinterStatusIcon(false);
                }
            }));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void SamplePrint()
    {
        try
        {
            //CommonFn.callLoading(true);
            ArrayList listData = new ArrayList();
            listData.Add(new byte[] { 27, 97, 49 });
            listData.Add(Encoding.UTF8.GetBytes("\nTest Co.Ltd\n"));
            listData.Add(Encoding.UTF8.GetBytes("Branch 000007"));
            listData.Add(new byte[] { 27, 97, 48 });
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("TM. : IN_A3           EXIT #0000\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("TAX ID. : 0107537002443\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("POS ID. : 0000000000000003\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("Rcpt ID. : 02160312345\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("LICENSE PLATE : กข1234\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("IN : 16-02-2016 09:25\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("OUT: 16-02-2016 10:25\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("Parking Fee          300 Baht\r\n"));
            //listData.Add(Encoding.UTF8.GetBytes("Penalty Fee           50.00 Baht\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("Stamp Code 310      -230 Baht\r\n"));
            //listData.Add(Encoding.UTF8.GetBytes("Coupon Discount      -30.00 Baht\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("Cash Received        100 Baht\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(new byte[] { 0x1b, 0x1d, 0x61, 0x01 }); // Alignment (center)
            listData.Add(Encoding.UTF8.GetBytes("VAT. INCLUDED\r\n"));

            listData.Add(new byte[] { 0x1b, 0x64, 0x02 }); // Cut
            listData.Add(new byte[] { 0x07 }); // Kick cash drawer
                                               // convert to byte[]
            int maxLen = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                byte[] dummyB = listData[i] as byte[];
                maxLen += dummyB.Length;
            }
            byte[] sendBytes = new byte[maxLen];
            int indexSendByte = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                byte[] dummyB = listData[i] as byte[];
                for (int y = 0; y < dummyB.Length; y++)
                {
                    sendBytes[indexSendByte] = dummyB[y];
                    indexSendByte++;
                }
            }

            //listData.Add(Encoding.UTF8.GetBytes("\nกขดาเสเสเสเส\n"));
            //				list.add("--------------------------------\r\n".getBytes());
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
            //ShareProfile.printerName = "BT:Star Micronics";
            string portName = "BT:00:15:0E:E5:7E:24";
            string result = ajc.CallStatic<string>("Print", portName, sendBytes);
            //string result = ajc.CallStatic<string>("TestPrint");
            Debug.Log("printer return message =  " + result);
        }
        catch (Exception e)
        {
            Debug.LogWarning("fail " + e);
            //CommonFn.callLoading(false);
        }

    }

    public static void Print(Input inputData, string portName, string scene, AndroidJavaObject activityContext)
    {
        try
        {
            //CommonFn.callLoading(true);
            ArrayList listData = new ArrayList();
            listData.Add(new byte[] { 10, 10, 10}); // Cut
            //listData.Add(new byte[] { 0x1b, 0x1d, 0x61, 0x01 });
            listData.Add(new byte[] { 27, 97, 49});
            listData.Add(Encoding.UTF8.GetBytes(inputData.siteName + "\n"));
            //listData.Add(Encoding.UTF8.GetBytes(inputData.address + "\n"));
            listData.Add(new byte[] { 27, 97, 48 });
            listData.Add(Encoding.UTF8.GetBytes("TM. : " + inputData.gateName + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("TAX ID. : " + inputData.taxId + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("POS ID. : " + inputData.posId + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("Rcpt ID. : " + inputData.receiptId + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("LICENSE PLATE : " + inputData.license + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("IN : " + inputData.enterDate + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("OUT: " + inputData.exitDate + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            string range = "{0,-17} {1,14}";

            //listData.Add(Encoding.UTF8.GetBytes(String.Format(range, "Penalty Fee", inputData.penaltyFee + " Baht") + "\r\n"));
            Debug.Log("Config.GetDevice(Constant.isReceiptDisplayDiscountLine) = " + Config.Get(Constant.isReceiptDisplayDiscountLine));
            var isPrintDiscountLine = bool.Parse(Config.Get(Constant.isReceiptDisplayDiscountLine));
            var parkingFee = inputData.parkingFee;
            if (isPrintDiscountLine)
            {
                listData.Add(Encoding.UTF8.GetBytes(String.Format(range, "Parking Fee", parkingFee + " Baht") + "\r\n"));
                listData.Add(Encoding.UTF8.GetBytes(String.Format(range, "Stamp " + inputData.stampCode, "-" + inputData.stampDiscount + " Baht") + "\r\n"));
            }
            else
            {
                parkingFee -= inputData.stampDiscount;
                listData.Add(Encoding.UTF8.GetBytes(String.Format(range, "Parking Fee", parkingFee + " Baht") + "\r\n"));
            }

            //listData.Add(Encoding.UTF8.GetBytes(String.Format(range, "Coupon Discount", inputData.couponDiscount + " Baht") + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(Encoding.UTF8.GetBytes(String.Format(range, "Cash Received", parkingFee + " Baht") + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(new byte[] { 27, 97, 49 });
            listData.Add(Encoding.UTF8.GetBytes("VAT. INCLUDED\r\n\r\n"));
            listData.Add(new byte[] { 10, 10, 10, 27, 122, 27, 121 }); // Cut
            //listData.Add(new byte[] { 0x07 }); // Kick cash drawer
            int maxLen = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                byte[] dummyB = listData[i] as byte[];
                maxLen += dummyB.Length;
            }
            byte[] sendBytes = new byte[maxLen];
            int indexSendByte = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                byte[] dummyB = listData[i] as byte[];
                for (int y = 0; y < dummyB.Length; y++)
                {
                    sendBytes[indexSendByte] = dummyB[y];
                    indexSendByte++;
                }
            }

            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
                //portName = "BT:00:15:0E:E5:7E:24";
                string result = ajc.CallStatic<string>("Print", portName, sendBytes);
                Debug.Log("printer return message =  " + result);

                if (result == Constant.printSuccess)
                {
                    if (scene == Constant.ExitGate)
                    {
                        GameObject MainEvent = GameObject.Find("_MainEvent");
                        ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).PrintResultCallBack(true);
                    }
                    else if (scene == Constant.Setting)
                    {
                        GameObject printerPanel = GameObject.Find("Setting/PrinterPanel");
                        ((PrinterSetting)printerPanel.GetComponent(typeof(PrinterSetting))).PrintResultCallBack(true);
                    }
                }
                else
                {
                    if (scene == Constant.ExitGate)
                    {
                        GameObject MainEvent = GameObject.Find("_MainEvent");
                        ((MainEventExitGate)MainEvent.GetComponent(typeof(MainEventExitGate))).PrintResultCallBack(false);
                    }
                    else if (scene == Constant.Setting)
                    {
                        GameObject printerPanel = GameObject.Find("Setting/PrinterPanel");
                        ((PrinterSetting)printerPanel.GetComponent(typeof(PrinterSetting))).PrintResultCallBack(false);
                    }
                }
            }));
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            throw ex;
        }
    }

    public static void PrintShift(ShiftInput inputData, string portName, AndroidJavaObject activityContext)
    {
        try
        {
            //CommonFn.callLoading(true);
            ArrayList listData = new ArrayList();
            listData.Add(new byte[] { 10, 10, 10 }); // Cut
            listData.Add(new byte[] { 27, 97, 49 });
            listData.Add(Encoding.UTF8.GetBytes(inputData.siteName + "\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(Encoding.UTF8.GetBytes(" CASHIER SUMMARY \r\n"));
            listData.Add(new byte[] { 27, 97, 48 });
            listData.Add(Encoding.UTF8.GetBytes("POS ID. : " + inputData.cashierNumber + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("SHIFT ID. : " + inputData.shiftId + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("IN : " + inputData.LoginTime + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("OUT: " + inputData.LogoutTime + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("--------------------------------\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("TOTAL CAR: " + inputData.totalCar + "\r\n"));
            listData.Add(Encoding.UTF8.GetBytes("TOTAL AMOUNT: " + inputData.totalAmount + "\r\n"));
            listData.Add(new byte[] { 10, 10, 10, 27, 122, 27, 121 }); // Cut

            int maxLen = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                byte[] dummyB = listData[i] as byte[];
                maxLen += dummyB.Length;
            }
            byte[] sendBytes = new byte[maxLen];
            int indexSendByte = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                byte[] dummyB = listData[i] as byte[];
                for (int y = 0; y < dummyB.Length; y++)
                {
                    sendBytes[indexSendByte] = dummyB[y];
                    indexSendByte++;
                }
            }

            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
                //portName = "BT:00:15:0E:E5:7E:24";
                string result = ajc.CallStatic<string>("Print", portName, sendBytes);
                Debug.Log("printer return message =  " + result);
                GameObject ShiftPanel = GameObject.Find("ShiftPanel");
                if (result == Constant.printSuccess)
                {
                    ((ShiftSetting)ShiftPanel.GetComponent(typeof(ShiftSetting))).PrintResultCallBack(true);
                }
                else
                {
                    ((ShiftSetting)ShiftPanel.GetComponent(typeof(ShiftSetting))).PrintResultCallBack(false);
                }
            }));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
