﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransactionSetting : MonoBehaviour {

    public Text LoginReserveTx;
    public Text EnterReserveTx;
    public Text ExitReserveTx;

    // Use this for initialization
    void Start () {
		
	}

    public void Initial()
    {
        try
        {
            List<string> exitTransactions = Transaction.ReadFolder(Constant.ExitGate);
            int exit = exitTransactions.Count - 1;
            if (exit <= 0)
            {
                exit = 0;
            }

            ExitReserveTx.text = "ทางออกคงค้าง : " + exit;
            List<string> enterTransactions = Transaction.ReadFolder(Constant.EnterGate);
            int enter = enterTransactions.Count - 1;
            if (enter <= 0)
            {
                enter = 0;
            }

            EnterReserveTx.text = "ทางเข้าคงค้าง : " + enter;
            List<string> LoginTransactions = Transaction.ReadFolder(Constant.Login);
            int login = LoginTransactions.Count - 1;
            if(login <= 0)
            {
                login = 0;
            }

            LoginReserveTx.text = "เข้า / ออกระบบคงค้าง : " + login;
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void ResendBtOnClicked()
    {
        try
        {
            Transaction.ResendFailTransaction();
            Initial();
            CommonFn.ShowToast(ToastrControl.toastrSuccess, "เริ่มทำการส่งข้อมูล ปิดหน้านี้แล้วเปิดใหม่อีกครั้ง");
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }
}
