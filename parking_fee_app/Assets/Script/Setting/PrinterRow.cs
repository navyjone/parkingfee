﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrinterRow : MonoBehaviour {

    // Use this for initialization
    public void OnClicked()
    {
        GameObject PrinterPanel = GameObject.Find("Setting/PrinterPanel");
        string printerName = this.gameObject.transform.GetChild(1).GetComponentInChildren<Text>().text;
        Debug.Log("OnClicked get printerName = " + printerName);
        ((PrinterSetting)PrinterPanel.GetComponent(typeof(PrinterSetting))).SelectPrinterCallBack(printerName);
    }
}
