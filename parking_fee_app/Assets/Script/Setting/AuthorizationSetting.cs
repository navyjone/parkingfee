﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthorizationSetting : MonoBehaviour {

    public GameObject Setting;
    public GameObject DevicePanel;
    public InputField passwordInput;
    private string password = "A4m1n";
    private string selectedPanel;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void WillOpenPanel(string panel)
    {
        this.selectedPanel = panel;
    }

    public void ConfirmBtOnClicked()
    {
        try
        {
            if (passwordInput.text == password)
            {
                passwordInput.text = "";
                //((Setting)Setting.GetComponent(typeof(Setting))).Initial();
                if (this.selectedPanel == Constant.deviceSetting)
                {
                    DevicePanel.SetActive(true);
                    ((DeviceSetting)DevicePanel.GetComponent(typeof(DeviceSetting))).Initial();
                }

                this.gameObject.SetActive(false);
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "ใส่รหัสผ่านไม่ถูกต้อง");
            }
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการยืนยันรหัสผ่าน");
        }
    }
}
