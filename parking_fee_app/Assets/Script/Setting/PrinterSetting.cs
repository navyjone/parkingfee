﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrinterSetting : MonoBehaviour {

    public GameObject contentScrollView;
    public GameObject SuccessModal;
    public GameObject FailModal;

    public struct Data
    {
        public string name;
        public string macAddress;
    }

    private string printerLongString = "";
    private string selectedPrinterMacAddress = "";
    private AndroidJavaObject activityContext = null;
    bool isDisplayPrinterRow = false;
    //private string listPrinterLongString = "";
    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(isDisplayPrinterRow)
        {
            isDisplayPrinterRow = false;
            string[] printers = this.printerLongString.Split(new string[] { "@!?;," }, StringSplitOptions.None);
            Debug.Log(printers);
            //last index is empty
            for (int i = 0; i < printers.Length - 1; i++)
            {
                string[] rowData = printers[i].Split(new string[] { "@!?;-" }, StringSplitOptions.None);
                Data data = new Data();
                Debug.Log("name=" + rowData[0]);
                Debug.Log("macAddress=" + rowData[1]);
                data.name = rowData[0].Replace("BT:", "");
                data.macAddress = "BT:" + rowData[1];
                AddRow(i, data, false);
            }
        }
	}

    public void Initial()
    {
        this.ClearChildObject(contentScrollView.transform);
        PrinterBluetooth.List(activityContext);
        //Data data = new Data();
        //data.name = "name1";
        //data.macAddress = "BT:11";
        //AddRow(0, data, false);
    }

    public void ConnectBtOnClicked()
    {
        SuccessModal.SetActive(true);
        Dictionary<string, string> inputSave = new Dictionary<string, string>();
        Debug.Log("ConnectBtOnClicked save printer = " + this.selectedPrinterMacAddress);
        inputSave.Add(Constant.activePrinter, this.selectedPrinterMacAddress);
        Config.Save(inputSave);
    }

    public void SuccessModalNextBtOnClicked()
    {
        SuccessModal.SetActive(false);
        this.gameObject.SetActive(false);
    }

    public void ListPrinterCallback(string printerLongString)
    {
        Debug.Log("printerLongString=" + printerLongString);
        this.printerLongString = printerLongString;
        this.isDisplayPrinterRow = true;
    }

    public void SelectPrinterCallBack(string printerMacAddress)
    {
        try
        {
            Debug.Log("SelectPrinterCallBack" + printerMacAddress);
            this.selectedPrinterMacAddress = printerMacAddress;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            // TODO: do something
        }
    }

    public void PrintResultCallBack(bool isSuccess)
    {
        try
        {
            CommonFn.ShowLoading(false);
            if (isSuccess)
            {
                CommonFn.ShowToast(ToastrControl.toastrSuccess, "พิมพ์ใบเสร็จรับเงินสำเร็จ");
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "พิมพ์ใบเสร็จรับเงินล้มเหลว");
            }
        }
        catch (Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการพิมพ์ใบเสร็จรับเงิน");
            Debug.LogError(ex.ToString());
        }
    }

    public void TestPrint()
    {
        try
        {
            CommonFn.ShowLoading(true);
            PrinterBluetooth.Input input = new PrinterBluetooth.Input();
            input.siteName = "Test Print";
            PrinterZKC.PrintTest();
            //PrinterBluetooth.Print(input, this.selectedPrinterMacAddress, Constant.Setting, activityContext);
        }
        catch(Exception ex)
        {
            CommonFn.ShowLoading(false);
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการทดสอบพิมพ์ใบเสร็จ");
        }
    }

    private void AddRow(int index, Data inputData, bool isOnline)
    {
        GameObject instance = Instantiate(Resources.Load("Prefabs/PrinterRow", typeof(GameObject))) as GameObject;
        instance.transform.SetParent(contentScrollView.transform);
        instance.transform.GetChild(0).GetComponentInChildren<Text>().text = inputData.name;
        instance.transform.GetChild(1).GetComponentInChildren<Text>().text = inputData.macAddress;
        if(isOnline)
        {
            //instance.transform.GetChild(2).GetComponentInChildren<Image>().color = Color.green;
        }

        if (inputData.macAddress == Config.Get(Constant.activePrinter))
        {
            Button button = instance.transform.GetComponent<Button>();
            ColorBlock colorBlock = button.colors;
            colorBlock.normalColor = new Color32(49, 172, 248, 255);
            button.colors = colorBlock;
        }

        instance.name = "PrinterRow" + index.ToString();
        //int height = (int)instance.GetComponent<RectTransform>().rect.height;
        instance.GetComponent<RectTransform>().offsetMax = new Vector2(0f, 0f);
        instance.GetComponent<RectTransform>().offsetMin = new Vector2(0f, -70);
        instance.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.0f, (float)(index * -80) - 50, 0.0f);
        instance.GetComponent<RectTransform>().localScale = Vector3.one;
        //TODO make right, left = 35
        //this_rect.anchorMin = new Vector2(0, 1);
        //this_rect.anchorMax = new Vector2(1, 1);
        //Vector2 temp = new Vector2(left, posY - (height / 2f));
        //this_rect.offsetMin = temp;
        //temp = new Vector3(-right, temp.y + height);
        //this_rect.offsetMax = temp;
        Resources.UnloadUnusedAssets();
    }

    private void ClearChildObject(Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

}
