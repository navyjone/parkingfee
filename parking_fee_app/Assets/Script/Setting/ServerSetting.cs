﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerSetting : MonoBehaviour {

    public Text LastSyncTx;
    public InputField ServerIPInput;
    private string lastSync = "";
    private bool displayLastSync = false;
    public delegate string AsyncMethodCaller();
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (this.displayLastSync)
        {
            this.displayLastSync = false;
            LastSyncTx.text = this.lastSync;
        }
    }

    public void Initial()
    {
        LastSyncTx.text = "เชื่อมต่อครั้งล่าสุด " + Config.Get(Constant.lastSync);
        ServerIPInput.text = Config.Get(Constant.serverIP);
    }

    public void SyncBtClicked()
    {
        try
        {
            CommonFn.ShowLoading(true);
            if (ServerIPInput.text == "")
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "โปรดตวรจสอบหมายเลข IP server");
                return;
            }

            Dictionary<string, string> inputSaveServerIP = new Dictionary<string, string>();
            inputSaveServerIP.Add(Constant.serverIP, ServerIPInput.text);
            Config.Save(inputSaveServerIP);

            AsyncMethodCaller caller = new AsyncMethodCaller(RequestAll);
            AsyncCallback completedCallback = new AsyncCallback((ar) =>
            {
                var result = caller.EndInvoke(ar);
                if (result == TcpConnection.Status.success)
                {
                    CommonFn.ShowToast(ToastrControl.toastrSuccess, "เชื่อมต่อข้อมูลกับส่วนกลางสำเร็จ");
                    Dictionary<string, string> inputSave = new Dictionary<string, string>();
                    string dateTime = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year.ToString() + " " +
                                      DateTime.Now.Hour.ToString("D2") + ":" + DateTime.Now.Minute.ToString("D2") + ":" + DateTime.Now.Second.ToString("D2");
                    inputSave.Add(Constant.lastSync, dateTime);
                    Config.Save(inputSave);
                    this.lastSync = "เชื่อมต่อครั้งล่าสุด " + dateTime;
                    this.displayLastSync = true;
                }
                else
                {
                    CommonFn.ShowToast(ToastrControl.toastrError, "เชื่อมต่อข้อมูลกับส่วนกลางล้มเหลว");
                    //Debug.LogError(result);
                }

                CommonFn.ShowLoading(false);
            });

            caller.BeginInvoke(new AsyncCallback(completedCallback), null);
        }
        catch (Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดเชื่อมต่อข้อมูลกับส่วนกลาง");
            Debug.LogError(ex.ToString());
        }
    }

    private string RequestAll()
    {
        var result = Config.RequestAll();
        return result;
    }
}
