﻿using Assets.Script.ExitGate;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShiftSetting : MonoBehaviour {

    public GameObject SuccessModal;
    public Text shiftIdTx;
    public Text loginTimeTx;
    public Text totalCarTx;
    public Text totalAmountTx;
    private AndroidJavaObject activityContext = null;
    private PrintData.ShiftInput shiftData;

    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	}

    public void Initial(PrintData.ShiftInput shiftInput)
    {
        shiftIdTx.text = "ID " + shiftInput.shiftId;
        loginTimeTx.text = "เวลาเข้ากะ " + shiftInput.LoginTime;
        totalCarTx.text = "จำนวนรถ " + shiftInput.totalCar;
        totalAmountTx.text = "จำนวนเงินที่รับ " + shiftInput.totalAmount;
        PrinterZKC.Init();
    }

    public void SubmitBtOnClicked()
    {
        // 1. print shift slip
        // 2. send transaction logout
        // 3. open success Modal
        var logOutTime = DateTime.Now;
        this.shiftData = Shift.PreparePrintInput(logOutTime);
        Print(this.shiftData);
        LogOut(logOutTime);
        SuccessModal.SetActive(true);
    }

    public void RePrintBtOnClicked()
    {
        Print(this.shiftData);
    }

    public void SuccessModalNextBtOnClicked()
    {
        SuccessModal.SetActive(false);
        this.gameObject.SetActive(false);
        SceneManager.LoadScene("Login");
    }

    public void PrintResultCallBack(bool isSuccess)
    {
        try
        {
            CommonFn.ShowLoading(false);
            if (isSuccess)
            {
                CommonFn.ShowToast(ToastrControl.toastrSuccess, "พิมพ์ใบสรุปกะสำเร็จ");
            }
            else
            {
                CommonFn.ShowToast(ToastrControl.toastrError, "พิมพ์ใบสรุปกะล้มเหลว");
            }
        }
        catch (Exception ex)
        {
            CommonFn.ShowLoading(false);
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการพิมพ์ใบสรุปกะ");
            Debug.LogError(ex.ToString());
        }
    }

    public void Print(PrintData.ShiftInput shiftInput)
    {
        try
        {
            CommonFn.ShowLoading(true);
            var printerMac = Config.Get(Constant.activePrinter);
            print("totalAmount = " + shiftInput.totalAmount);
            print("totalCar = " + shiftInput.totalCar);
            PrinterZKC.PrintShift(shiftInput);
            CommonFn.ShowLoading(false);
            //PrinterBluetooth.PrintShift(shiftInput, printerMac, activityContext);
        }
        catch(Exception ex)
        {
            CommonFn.ShowLoading(false);
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการทดสอบพิมพ์ใบสรุปกะ");
        }
    }

    public void LogOut(DateTime logOutTime)
    {
        try
        {
            Transaction.Login[] transaction = new Transaction.Login[1];
            transaction[0] = new Transaction.Login();
            string lastLogin = Config.Get(Constant.lastLogin);
            if (lastLogin != "")
            {
                transaction[0].loginDatetime = DateTime.Parse(lastLogin);
            }
            else
            {
                transaction[0].loginDatetime = DateTime.MinValue;
            }

            transaction[0].logoutDatetime = logOutTime;
            transaction[0].gateId = int.Parse(Config.Get(Constant.activeGateId));
            transaction[0].userId = int.Parse(Config.Get(Constant.activeUserId));
            Transaction.Send(transaction, Constant.Login);
            Dictionary<string, string> inputSave = new Dictionary<string, string>();
            inputSave.Add(Constant.activeUserId, "");
            inputSave.Add(Constant.lastLogin, "");
            inputSave.Add(Constant.activeGateId, "");
            Config.Save(inputSave);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการออกจากระบบ");
        }
    }
}
