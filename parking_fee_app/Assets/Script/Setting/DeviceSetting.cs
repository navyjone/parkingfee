﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeviceSetting : MonoBehaviour {

    public InputField enterGateIdInput;
    public InputField exitGateIdInput;
    public InputField taxIdInput;
    public InputField receiptInput;
    public InputField apiUrlInput;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Initial()
    {
        try
        {
            enterGateIdInput.text = Config.Get(Constant.enterGateId);
            exitGateIdInput.text = Config.Get(Constant.exitGateId);
            taxIdInput.text = Config.dtSysteminfo.Rows[0][Constant.taxId].ToString();
            receiptInput.text = Config.Get(Constant.receiptNumber);
            apiUrlInput.text = Config.Get(Constant.apiUrl);
        }
        catch(Exception ex)
        {
            Debug.LogError("inital Device setting fail " + ex.ToString());
        }
    }

    public void SaveBtOnClicked()
    {
        Dictionary<string, string> inputSave = new Dictionary<string, string>();
        inputSave.Add(Constant.enterGateId, enterGateIdInput.text);
        inputSave.Add(Constant.exitGateId, exitGateIdInput.text);
        inputSave.Add(Constant.receiptNumber, receiptInput.text);
        inputSave.Add(Constant.apiUrl, apiUrlInput.text);
        Config.Save(inputSave);
        CommonFn.ShowToast(ToastrControl.toastrSuccess, "บันทีกสำเร็จ");
    }
}
