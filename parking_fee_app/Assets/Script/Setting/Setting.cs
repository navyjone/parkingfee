﻿using Assets.Script.ExitGate;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Setting : MonoBehaviour {
	
	public GameObject ServerPanel;
	public GameObject DevicePanel;
	public GameObject PrinterPanel;
	public GameObject TrancsactionPanel;
	public GameObject MainPanel;
	public GameObject LogOffModal;
    public GameObject AuthorizationPanel;
    public GameObject ShiftPanel;
    public GameObject LogOutBt;

    System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
    private bool doListPrinter = false;

    void Update()
    {
        if (doListPrinter)
        {
            doListPrinter = false;
        }
    }

    public void Initial()
	{
		this.gameObject.SetActive(true);
        AuthorizationPanel.SetActive(false);
        MainPanel.SetActive(true);
		ServerPanel.SetActive(false);
		DevicePanel.SetActive(false);
		PrinterPanel.SetActive(false);
        TrancsactionPanel.SetActive(false);
        ShiftPanel.SetActive(false);
        var currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == Constant.Login)
        {
            LogOutBt.SetActive(false);
        }
        else
        {
            LogOutBt.SetActive(true);
        }
    }

    //public void InitialWithAuthorization()
    //{
    //    this.gameObject.SetActive(true);
    //    AuthorizationPanel.SetActive(true);
    //    MainPanel.SetActive(false);
    //    ServerPanel.SetActive(false);
    //    DevicePanel.SetActive(false);
    //    PrinterPanel.SetActive(false);
    //    TrancsactionPanel.SetActive(false);
    //}

	public void ServerBtClick()
	{
        ServerPanel.SetActive(true);
        ((ServerSetting)ServerPanel.GetComponent(typeof(ServerSetting))).Initial();
    }

    public void DeviceBtClick()
	{
        AuthorizationPanel.SetActive(true);
        ((AuthorizationSetting)AuthorizationPanel.GetComponent(typeof(AuthorizationSetting))).WillOpenPanel(Constant.deviceSetting);
        //DevicePanel.SetActive(true);
        //((DeviceSetting)DevicePanel.GetComponent(typeof(DeviceSetting))).Initial();
    }

	public void PrinterBtClick()
	{
		PrinterPanel.SetActive(true);
        ((PrinterSetting)PrinterPanel.GetComponent(typeof(PrinterSetting))).Initial();
    }

	public void TransactionBtClick()
	{
		TrancsactionPanel.SetActive(true);
        ((TransactionSetting)TrancsactionPanel.GetComponent(typeof(TransactionSetting))).Initial();
    }

	public void BackBtClick()
	{
		this.gameObject.SetActive(false);
	}

	public void LogOutBtClick()
	{
		LogOffModal.SetActive(true);
	}

    public void ConfirmLogOutBtOnClicked()
    {
        var currentScene = SceneManager.GetActiveScene();
        if(currentScene.name == Constant.ExitGate)
        {
            //exit gate has to open shift panel
            var shiftData = Shift.PreparePrintInput(DateTime.Now);
            if(shiftData.shiftId == "")
            {
                LogOut();
            }
            else
            {
                ShiftPanel.SetActive(true);
                ((ShiftSetting)ShiftPanel.GetComponent(typeof(ShiftSetting))).Initial(shiftData);
            }
        }
        else
        {
            LogOut();
        }
    }


	private void LogOut()
	{
        try
        {
            Transaction.Login[] transaction = new Transaction.Login[1];
            transaction[0] = new Transaction.Login();
            string lastLogin = Config.Get(Constant.lastLogin);
            if (lastLogin != "")
            {
                transaction[0].loginDatetime = DateTime.Parse(lastLogin);
            }
            else
            {
                transaction[0].loginDatetime = DateTime.MinValue;
            }

            transaction[0].logoutDatetime = DateTime.Now;
            transaction[0].gateId = int.Parse(Config.Get(Constant.activeGateId));
            transaction[0].userId = int.Parse(Config.Get(Constant.activeUserId));
            Transaction.Send(transaction, Constant.Login);
            Dictionary<string, string> inputSave = new Dictionary<string, string>();
            inputSave.Add(Constant.activeUserId, "");
            inputSave.Add(Constant.lastLogin, "");
            inputSave.Add(Constant.activeGateId, "");
            Config.Save(inputSave);
            SceneManager.LoadScene("Login");
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาดในการออกจากระบบ");
        }
	}

	public void Close(GameObject Obj)
	{
		Obj.SetActive(false);
	}
}
