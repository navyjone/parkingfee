﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEventPlayGround : MonoBehaviour {

    public delegate string[] AsyncListPrinter();
    public delegate string AsyncGetStatusPrinter();

    // Use this for initialization
    void Start () {
    }

    private bool doListPrinter = false;
    private AndroidJavaObject activityContext = null;

    void Update()
    {
        if (doListPrinter)
        {
            doListPrinter = false;
        }

        if (Application.platform == RuntimePlatform.Android)
        {

            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {

                // Quit the application
                Application.Quit();
            }
        }
    }

    public void CheckStatus()
    {
        PrinterBluetooth.GetStatus("BT:00:15:0E:E5:7E:24");
    }

    public void List()
    {
        //string[] printerNames = PrinterBluetooth.List();
        //if(printerNames != null)
        //{
        //    for (int i = 0; i < printerNames.Length; i++)
        //    {
        //        Debug.Log(printerNames[i]);
        //    }
        //}
    }

    public void Print()
    {
        PrinterBluetooth.Input printerData = new PrinterBluetooth.Input();
        printerData.enterDate = "20/02/01";
        printerData.exitDate = "21/02/01";
        printerData.license = "license";
        printerData.parkingFee = 123;
        printerData.stampCode = "001";
        printerData.stampDiscount = 0;
        printerData.siteName = "Central Rama III";
        printerData.posId = "00000POS01";
        printerData.receiptId = "REP001203";
        PrinterBluetooth.Print(printerData, Config.Get(Constant.activePrinter), Constant.ExitGate, activityContext);
    }

    public void ListAsync()
    {
        Debug.Log("ListAsync");
        AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);

        using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        }

        activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
            string result = ajc.CallStatic<string>("List");
            Debug.Log("SelectPrinter =  " + result);
        }));
        //ajc.Call("List", new AndroidJavaRunnable(runOnUiThread));
    }

    void runOnUiThread()
    {
        Debug.Log("I'm running on the Java UI thread!");
    }

    public void CheckStatusAsync()
    {
        Debug.Log("CheckStatusAsync");
        AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinter);
 
     
        using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        }

        activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
            string result = ajc.CallStatic<string>("Status", "BT:00:15:0E:E5:7E:24");
            Debug.Log("CheckStatusAsync =  " + result);
        }));
    }

    private string[] ListPrinter()
    {
        string[] result = new string[] { "", "" };
        //result = PrinterBluetooth.List();
        return result;
    }

    private string GetStatusPrinter()
    {
        return PrinterBluetooth.GetStatus(Config.Get(Constant.activePrinter));
    }

    public void PrintZKC()
    {
        Debug.Log("PrintZKC");
        var ExitGate = new PrintData.ExitGateInput
        {
            enterDate = "20/02/01",
            exitDate = "21/02/01",
            license = "license",
            parkingFee = 123,
            stampCode = "001",
            stampDiscount = 0,
            siteName = "Central Rama III",
            posId = "00000POS01",
            receiptId = "REP001203",
            address = "address",
            cashReceive = 456,
            gateName = "gateName",
            couponDiscount = 789,
            taxId = "0123034234234"
        };

        PrinterZKC.PrintExit(ExitGate);
    }

    public void GetStatusZKC()
    {
        string result = PrinterZKC.GetStatus();
        Debug.Log("GetStatusZKC" + result);
    }

    public void InitZKC()
    {
        Debug.Log("InitZKC");
        PrinterZKC.Init();
    }

    public void JavaMessage(string result)
    {
        Debug.Log("JavaSendMessage = " + result);
    }

    public void SetAct()
    {
        try
        {
            var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var unityActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            ajc.CallStatic("receiveActivityInstance", unityActivity);
        }
        catch (Exception ex)
        {
            print(ex.ToString());
        }
    }

    public void TestReturnString()
    {
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(Constant.pathJavaObjPrinterZKC);
            string result1 = ajc.Call<string>("ReturnStr");
            string result2 = ajc.CallStatic<string>("ReturnStaticString");
            string result3 = ajc.CallStatic<string>("TestSendMsg");
        }
        catch (Exception ex)
        {
            print(ex.ToString());
        }
    }
}
