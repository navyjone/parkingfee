﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectMode : MonoBehaviour {

    void Start()
    {
    }

    // Use this for initialization
    public void EnterGateClick()
	{
        //SceneManager.LoadScene("EnterGate", LoadSceneMode.Additive);
        //SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        try
        {
            this.SendLoginTransaction(Constant.EnterGate);
            SceneManager.LoadScene(Constant.EnterGate);
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาในการเลือกทางเข้า/ออก");
        }
    }

	public void ExitGateClick()
	{
        //SceneManager.LoadScene("ExitGate", LoadSceneMode.Additive);
        //SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        try
        {
            this.SendLoginTransaction(Constant.ExitGate);
            SceneManager.LoadScene(Constant.ExitGate);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            CommonFn.ShowToast(ToastrControl.toastrError, "พบความผิดพลาในการเลือกทางเข้า/ออก");
        }
    }

	public void ColorWhite(GameObject button)
	{
		button.transform.Find("CarImg").GetComponent<Image>().color = new Color32(255,255,255,255);
		button.transform.Find("ArrowImg").GetComponent<Image>().color = new Color32(255,255,255,255);
		button.transform.Find("Text").GetComponent<Text>().color = new Color32(255,255,255,255);
	}

    private void SendLoginTransaction(string gate)
    {
        try
        {
            //Parking API receive array 
            Transaction.Login[] transaction = new Transaction.Login[1];
            transaction[0] = new Transaction.Login();
            transaction[0].loginDatetime = DateTime.Now;
            transaction[0].logoutDatetime = DateTime.MinValue;
            if (gate == Constant.EnterGate)
            {
                transaction[0].gateId = int.Parse(Config.Get(Constant.enterGateId));
            }
            else if (gate == Constant.ExitGate)
            {
                transaction[0].gateId = int.Parse(Config.Get(Constant.exitGateId));
            }

            transaction[0].userId = int.Parse(Config.Get(Constant.activeUserId));
            Transaction.Send(transaction, Constant.Login);
            //save loginDateTime for send Logout API
            string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Dictionary<string, string> inputSave = new Dictionary<string, string>
            {
                { Constant.lastLogin, dateTime },
                { Constant.activeGateId, transaction[0].gateId.ToString() }
            };
            Config.Save(inputSave);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
}

